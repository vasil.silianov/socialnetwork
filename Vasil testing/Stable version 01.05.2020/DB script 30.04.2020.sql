-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.67-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for social_network
DROP DATABASE IF EXISTS `social_network`;
CREATE DATABASE IF NOT EXISTS `social_network` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `social_network`;

-- Dumping structure for table social_network.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `username_authority` (`username`,`authority`),
  CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.authorities: ~12 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('dionisiev', 'ROLE_USER'),
	('Eti', 'ROLE_USER'),
	('gekul', 'ROLE_USER'),
	('gogo', 'ROLE_ADMIN'),
	('gogo', 'ROLE_USER'),
	('iliq', 'ROLE_USER'),
	('mimi', 'ROLE_USER'),
	('radi', 'ROLE_ADMIN'),
	('radi', 'ROLE_USER'),
	('Tedi', 'ROLE_USER'),
	('teo', 'ROLE_USER'),
	('vas', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table social_network.comments
DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment_text` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`comment_id`),
  KEY `comments_posts_post_id_fk` (`post_id`),
  KEY `comments_users_user_id_fk` (`user_id`),
  CONSTRAINT `comments_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `comments_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.comments: ~1 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `post_id`, `user_id`, `comment_text`, `date_created`, `enabled`) VALUES
	(1, 2, 5, 'Nice post', '2020-04-12 18:09:33', 1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table social_network.likes
DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `likes_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`likes_id`),
  KEY `likes_posts_post_id_fk` (`post_id`),
  KEY `likes_users_user_id_fk` (`user_id`),
  CONSTRAINT `likes_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `likes_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;

-- Dumping structure for table social_network.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_text` text,
  `enabled` tinyint(4) DEFAULT '1',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `public` tinyint(4) DEFAULT '1',
  `image` mediumblob,
  PRIMARY KEY (`post_id`),
  KEY `posts_users_user_id_fk` (`user_id`),
  CONSTRAINT `posts_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.posts: ~6 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `user_id`, `post_text`, `enabled`, `date_created`, `public`, `image`) VALUES
	(2, 5, 'First post', 1, '2020-04-11 13:05:42', 1, NULL),
	(3, 5, 'Second post', 1, '2020-04-11 16:06:47', 1, NULL),
	(4, 28, 'cdgfdg', 0, '2020-04-30 14:47:37', 1, NULL),
	(5, 31, 'dfsd', 0, '2020-04-30 17:56:19', 1, NULL),
	(6, 28, 'fdsgfd', 0, '2020-04-30 20:59:38', 1, NULL),
	(7, 28, 'dfdfsd', 0, '2020-04-30 21:00:13', 1, NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table social_network.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.users: ~10 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('dionisiev', '$2a$10$BFHze2O7pMkOywrzkLANIurAhANl3xhocwOZ2cclxnrflRpUthy2C', 1),
	('Eti', '$2a$10$z1DQfFBYC/praQwL9JyNBezaX9ZHoqKK8KXVWyVfqiQXof/N2hfmK', 1),
	('gekul', '1234', 0),
	('gogo', 'gogo', 0),
	('iliq', '$2a$10$52LGMTvbxgB3Rx/zFPLH/.zrJ5PIv.et0Xc8l6kYf0uKCOHsRVPDO', 1),
	('mimi', '$2a$10$uW5GW/WWEOsOD7v7UPXUCOqDoygoKdEdxsbheOfkILturxpdAlc92', 1),
	('radi', '$2a$10$L.AJOupWfegm5uSVIRnnJ.ZaiAUbHGam83AbRUwK5YnGv3qVOlC9C', 1),
	('Tedi', '12345', 0),
	('teo', '$2a$10$M9WlGVf9rZb41El2fVYoJ.wnaoawbwZw7DNvffTL83WUUMTcHXWLS', 1),
	('vas', '123', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table social_network.users_information
DROP TABLE IF EXISTS `users_information`;
CREATE TABLE IF NOT EXISTS `users_information` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(68) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  `picture` mediumblob,
  PRIMARY KEY (`user_id`),
  KEY `users_old_users_username_fk` (`username`),
  CONSTRAINT `users_old_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.users_information: ~10 rows (approximately)
/*!40000 ALTER TABLE `users_information` DISABLE KEYS */;
INSERT INTO `users_information` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `enabled`, `picture`) VALUES
	(5, 'Georgi', 'Kulov', 'gekul', '1234', 'kulov@suck.it', 0, NULL),
	(23, NULL, NULL, 'gogo', 'gogo', NULL, 0, NULL),
	(24, NULL, NULL, 'mimi', 'mimi', NULL, 1, NULL),
	(25, 'Ekaterina', 'moma', 'Eti', '12345', 'eti.sexa@gmail.com', 1, NULL),
	(26, 'Teodora', 'moma', 'Tedi', '12345', 'tedi.sexa@gmail.com', 0, NULL),
	(27, NULL, NULL, 'vas', '123', NULL, 0, NULL),
	(28, NULL, NULL, 'iliq', 'qwerty', NULL, 1, NULL),
	(29, NULL, NULL, 'dionisiev', '1234', NULL, 1, NULL),
	(30, NULL, NULL, 'teo', '123', NULL, 1, NULL),
	(31, NULL, NULL, 'radi', 'asd', NULL, 1, NULL);
/*!40000 ALTER TABLE `users_information` ENABLE KEYS */;

-- Dumping structure for table social_network.user_relationship
DROP TABLE IF EXISTS `user_relationship`;
CREATE TABLE IF NOT EXISTS `user_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester` int(11) NOT NULL,
  `requestee` int(11) NOT NULL,
  `status` varchar(29) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_relationship_pk_2` (`requester`,`requestee`),
  KEY `user_relationship_users_information_user_id_fk_2` (`requestee`),
  CONSTRAINT `user_relationship_users_information_user_id_fk` FOREIGN KEY (`requester`) REFERENCES `users_information` (`user_id`),
  CONSTRAINT `user_relationship_users_information_user_id_fk_2` FOREIGN KEY (`requestee`) REFERENCES `users_information` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.user_relationship: ~10 rows (approximately)
/*!40000 ALTER TABLE `user_relationship` DISABLE KEYS */;
INSERT INTO `user_relationship` (`id`, `requester`, `requestee`, `status`) VALUES
	(1, 27, 28, 'Approved'),
	(2, 28, 27, 'Approved'),
	(6, 26, 25, 'Waiting'),
	(7, 25, 26, 'Pending'),
	(8, 30, 29, 'Approved'),
	(9, 29, 30, 'Approved'),
	(10, 30, 28, 'Approved'),
	(11, 28, 30, 'Approved'),
	(12, 30, 27, 'Approved'),
	(13, 27, 30, 'Approved');
/*!40000 ALTER TABLE `user_relationship` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
