package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

//    List<Comment> findAllByPostId(int postID);

}
