package com.example.socialnetwork.controllers;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.ImageService;
import com.example.socialnetwork.services.contracts.PostService;
import com.example.socialnetwork.services.contracts.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

@Controller
public class  PostController {

    private UserService userService;
    private PostService postService;
    private ImageService imageService;

    @Autowired
    public PostController(UserService userService, PostService postService, ImageService imageService) {
        this.userService = userService;
        this.postService = postService;
        this.imageService = imageService;
    }


//    @GetMapping("/userfeed")
//    public String getPosts(Model model) {
//        model.addAttribute("newpost", new Post());
//        return "userfeed";
//    }


    @PostMapping("/userfeed/post/new")
    public String createPost(@Valid @ModelAttribute Post post, BindingResult bindingResult,
                             @RequestParam MultipartFile file,
                             Model model, Principal principal) throws IOException {

        UserInformation user = userService.getByUserName(principal.getName());
        model.addAttribute("file", file);

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Post can't be empty!");
            return "redirect:/userfeed";
        }


        post.setImage(imageService.convertImage(file));
        postService.createPost(post, user.getUserName());
        return "redirect:/userfeed";
    }

    @GetMapping("/post/{id}/image")
    public void renderPostPicture(@PathVariable int id, HttpServletResponse response) throws IOException {
        Post post = postService.getPostById(id);

        if (post.getImage() != null) {
            byte[] byteArray = new byte[post.getImage().length];
            int i = 0;

            for (Byte wrappedByte : post.getImage()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

    }

