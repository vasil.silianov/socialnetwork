package com.example.socialnetwork.services.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

public interface ImageService {
    void saveImage(int id, MultipartFile file);

    Byte[] convertImage(MultipartFile image) throws IOException;
}
