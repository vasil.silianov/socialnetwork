package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;

import java.util.List;

public interface PostService {
    List<Post> getAll();

    List<PostDTO> getAllDto();

    Post getPostById(int postID);

     Post createPost(Post post, String username);

     void deletePostByID(int postID);

     List<Post> getPostByUsername(String username);

     List<Post> postsCreatedBy(UserInformation user);

    int likePost(int postID, String username);

    int unlikePost(int postID, String username);

    List<Post> getLatest3DaysPosts();
}
