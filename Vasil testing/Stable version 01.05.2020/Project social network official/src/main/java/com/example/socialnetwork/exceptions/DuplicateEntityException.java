package com.example.socialnetwork.exceptions;

public class DuplicateEntityException extends RuntimeException{
    public DuplicateEntityException(String msg){
        super(msg);
    }
}
