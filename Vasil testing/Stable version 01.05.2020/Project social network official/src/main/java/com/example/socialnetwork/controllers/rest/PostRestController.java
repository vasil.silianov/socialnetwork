package com.example.socialnetwork.controllers.rest;


import com.example.socialnetwork.exceptions.DuplicateEntityException;
import com.example.socialnetwork.exceptions.EntityBadRequestException;
import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.PostService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;
    private UserService userService;

    @Autowired
    public PostRestController(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }

    @GetMapping()
    public List<Post> getAllPosts() {
            return postService.getAll();
    }

    @GetMapping("/dto")
    public List<PostDTO> getAllPostsDto() {

            return postService.getAllDto();

    }

    @GetMapping("/{postID}")
    public Post getPostById(@PathVariable int postID) {
        try {
            return postService.getPostById(postID);
        } catch (PostDoesNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/{postID}")
    private void deletePostByID(@PathVariable int postID) {

        try {
            postService.deletePostByID(postID);

        } catch (PostDoesNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping()
    public void createPost(@Valid @RequestBody Post post, Principal principal) {
        UserInformation user = userService.getByUserName(principal.getName());
        try {
            postService.createPost(post, user.getUserName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityBadRequestException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @PutMapping("/{postID}/like")
    public int addPostLike(@PathVariable int postID, Principal principal){

        try{
            return postService.likePost(postID,principal.getName());
        }catch (EntityNotFoundException | DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{postID}/unlike")
    public int unlikePost(@PathVariable int postID, Principal principal){
        try{
            return postService.unlikePost(postID,principal.getName());
        }catch (EntityNotFoundException | DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/viral")
    public Post currentViralPost(){
        List<Post> lastFewPosts = postService.getLatest3DaysPosts();
        int totalSum;
        int biggest =0;
        Post viral = new Post() ;
        for (int i = 0; i <lastFewPosts.size() ; i++) {
            totalSum=0;
            totalSum+=lastFewPosts.get(i).getLikes().size();
            totalSum+=lastFewPosts.get(i).getComments().size();
            if (totalSum>biggest){
                biggest=totalSum;
                viral = lastFewPosts.get(i);
            }
        }
        return viral;
    }
}
