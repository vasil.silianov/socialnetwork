package com.example.socialnetwork.controllers;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import  org.springframework.security.core.userdetails.User;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@Controller
//@RequestMapping
public class AdminController {

    UserService userService;
    UserDetailsManager userDetailsManager;
    UserRepository userRepository;

    @Autowired
    public AdminController(UserRepository userRepository,UserService userService,UserDetailsManager userDetailsManager) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.userRepository = userRepository;
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }


    @GetMapping("/admin/users")
    public  String allUser(Model model){
        model.addAttribute("users",userService.getAll());
        return "admin-panel-for-users";
    }

    @GetMapping("/admin/user/{name}")
    public String disable( @PathVariable String name){
        UserInformation userInformation = userService.getByUsername(name);
        userService.disableUser(userInformation.getId());
        User  user = (User) userDetailsManager.loadUserByUsername(name);// new User();
        return "admin";
    }

    @GetMapping("/admin/user/enable/{name}")
    public String enable( @PathVariable String name){
        UserInformation userInformation = userService.getByUsername(name);
        userService.enableUser(userInformation.getId());
        User  user = (User) userDetailsManager.loadUserByUsername(name);// new User();
        return "admin";
    }

}
