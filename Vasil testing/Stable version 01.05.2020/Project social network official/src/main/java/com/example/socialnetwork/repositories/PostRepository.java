package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> findAllByUserUserName(String username);

  //  List<Post> postsCreatedBy(int id);

    List<Post> findAllByUserOrderByCreatedDateDesc(UserInformation user);

    List<Post> findAllByOrderByCreatedDateDesc();

    List<Post> findPostsByCreatedDateIsAfter(Date dateBeginningOfPosts);//(int day1, int day2);//CreatedDateIsBetween_DayIsLessThanEqual(int days);//getAllLastTreeDays(@Param("timeDistance") Data timeDistance);

//
//    Optional<Post> getByID(int id);
}
