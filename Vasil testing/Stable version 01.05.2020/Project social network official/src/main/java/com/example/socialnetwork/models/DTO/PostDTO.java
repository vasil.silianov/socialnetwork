package com.example.socialnetwork.models.DTO;

import com.example.socialnetwork.models.UserInformation;
import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

public class PostDTO {


    private int postId;
    @NotNull
    @Size(min = 2, max = 150, message = "You need to write something")
    private String postText;

    private int visibility;

    private UserInformation user;

    private Byte[] image;



    public PostDTO() {
    }

    public int getPostId() {
        return postId;
    }

    public Byte[] getImage() {
        return image;
    }

    public int getVisibility() {
        return visibility;
    }

    public UserInformation getUser() {
        return user;
    }

    public String getPostText() {
        return postText;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public void setUser(UserInformation user) {
        this.user = user;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
