package com.example.socialnetwork.models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int commentId;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @OneToOne
    @JoinColumn(name = "user_id")
    private UserInformation user;

    @Size(min = 2, max = 150, message = "You need to write something")
    @Column(name = "comment_text")
    private String commentText;

    @Column(name="date_created")
    private Date dateCreated;

    @Column(name = "enabled")
    private int enabled;



    public Comment() {
    }

    public Comment(int commentId, Post post, UserInformation user, String commentText) {
        this.commentId = commentId;
        this.post = post;
        this.user = user;
        this.commentText = commentText;
    }

    public UserInformation getUser() {
        return user;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public int getEnabled() {
        return enabled;
    }

    public Post getPost() {
        return post;
    }

    public int getCommentId() {
        return commentId;
    }

    public Post getPostId() {
        return post;
    }

    public UserInformation getUserId() {
        return user;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public void setPostId(Post postId) {
        this.post = postId;
    }

    public void setUserId(UserInformation userId) {
        this.user = userId;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public void setUser(UserInformation user) {
        this.user = user;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
