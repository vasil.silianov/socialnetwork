package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.DuplicateEntityException;
import com.example.socialnetwork.exceptions.EntityBadRequestException;
import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    public static final String POST_NOT_FOUND = "There isn't any post with ID %d";
    public static final String USER_NOT_FOUND = "Username doesn't exists!";
    public static final String DUPLICATE_LIKE = "Post already liked!";
    public static final String NO_LIKE_FOUND = "Like doesn't exists!";


    private PostRepository postRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public List<Post> getAll() {
        return postRepository.findAllByOrderByCreatedDateDesc();
    }

    @Override
    public List<PostDTO> getAllDto() {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(post -> modelMapper.map(post, PostDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Post getPostById(int postID) {
        Optional<Post> post = postRepository.findById(postID);
        if (!post.isPresent()) {
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND, postID));
        }
        return post.get();
    }

    @Override
    public Post createPost(Post post, String username) {
        UserInformation user = userRepository.findByUserName(username);
        post.setUser(user);
        post.setVisibility(1);
        post.setEnabled(1);
        post.setCreatedDate(Date.from(Instant.now()));
        PostDTO postDTO = new PostDTO();
//        if (postDTO.getImage() != null) {
//            Byte[] image = postDTO.getImage();
//            post.setImage(image);
//        }

        return postRepository.save(post);
    }

    @Override
    public void deletePostByID(int postID) {
        Optional<Post> newPost = postRepository.findById(postID);
        if (!newPost.isPresent()) {
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND, postID));
        }
        postRepository.deleteById(postID);
    }

    @Override
    public List<Post> getPostByUsername(String username) {

        return postRepository.findAllByUserUserName(username);

    }


    @Override
    public List<Post> postsCreatedBy(UserInformation user) {
        return postRepository.findAllByUserOrderByCreatedDateDesc(user);
    }

    @Override
    public int likePost(int postID, String username) {
        Post post = getPostById(postID);
        UserInformation user = userRepository.findByUserName(username);

        if (post.getLikes().stream().anyMatch(x -> x.getId() == user.getId())) {
            throw new DuplicateEntityException(DUPLICATE_LIKE);
        }

        post.getLikes().add(user);
        return postRepository.save(post).getLikes().size();

    }

    @Override
    public int unlikePost(int postID, String username) {
        Post post = getPostById(postID);
        UserInformation user = userRepository.findByUserName(username);

        if(post.getLikes().stream().noneMatch(x -> x.getId() == user.getId())){
            throw new EntityNotFoundException(NO_LIKE_FOUND);
        }
        post.getLikes().remove(user);
        return postRepository.save(post).getLikes().size();
    }

    public List<Post> getLatest3DaysPosts() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -3);
        Date beginningOfTime = now.getTime();
        return postRepository.findPostsByCreatedDateIsAfter(beginningOfTime);
    }
}
