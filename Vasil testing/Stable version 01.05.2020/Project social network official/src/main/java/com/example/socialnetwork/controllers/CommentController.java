package com.example.socialnetwork.controllers;


import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.CommentService;
import com.example.socialnetwork.services.contracts.PostService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class CommentController {

    private CommentService commentService;
    private UserService userService;
    private PostService postService;

    @Autowired
    public CommentController(CommentService commentService, UserService userService, PostService postService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
    }


    @PostMapping("/userfeed/{postID}/comments/new")
    public String createComment(@PathVariable int postID, @Valid @ModelAttribute Comment comment,
                                BindingResult bindingResult, Principal principal) {
        UserInformation user = userService.getByUserName(principal.getName());
        Post post = postService.getPostById(postID);

        if (bindingResult.hasErrors()) {
            return "redirect:/userfeed";
        }

        commentService.createComment(comment, user, post);
        return "redirect:/userfeed";
    }

}
