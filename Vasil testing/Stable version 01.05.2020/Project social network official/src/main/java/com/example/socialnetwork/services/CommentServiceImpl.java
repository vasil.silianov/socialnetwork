package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.EntityBadRequestException;
import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.CommentRepository;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {


    public static final String COMMENT_CANNOT_BE_EMPTY = "Comment can't be empty";
    public static final String COMMENT_NOT_FOUND = "Comment with an ID %d doesn't exists";

    private CommentRepository commentRepository;
    private UserRepository userRepository;
    private PostRepository postRepository;


    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, UserRepository userRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }




    @Override
    public Comment createComment(Comment comment, UserInformation user, Post post) {
        comment.setUser(user);
        comment.setDateCreated(Date.from(Instant.now()));
        comment.setPost(post);
        comment.setUser(user);
        comment.setEnabled(1);
        if(comment.getCommentText().isEmpty()){
            throw new EntityBadRequestException(COMMENT_CANNOT_BE_EMPTY);
        }
        return commentRepository.saveAndFlush(comment);
    }




    @Override
    public Comment getCommentByID(int commentId) {
        Optional<Comment> comment = commentRepository.findById(commentId);
        if(!comment.isPresent()){
            throw new EntityBadRequestException(COMMENT_NOT_FOUND);
        }
        return comment.get();
    }



    @Override
    public void deleteComment(Comment comment) {

        if(comment.getCommentText().isEmpty()){
            throw new EntityBadRequestException(COMMENT_NOT_FOUND);
        }
         commentRepository.delete(comment);
    }

    @Override
    public List<Comment> getComment() {
        return commentRepository.findAll();
    }


    @Override
    public Comment updateComment(int commentID, Comment comment) {
        Comment newComment = getCommentByID(commentID);
        if(newComment.getCommentText().isEmpty()){
            throw new EntityBadRequestException(COMMENT_CANNOT_BE_EMPTY);
        }
        newComment.setCommentText(comment.getCommentText());
        newComment.setDateCreated(Date.from(Instant.now()));
        return commentRepository.save(newComment);

    }

//    public List<Comment> getAllCommentsByPostId(int postID){
//
//        return commentRepository.findAllByPostId(postID);
//    }



}
