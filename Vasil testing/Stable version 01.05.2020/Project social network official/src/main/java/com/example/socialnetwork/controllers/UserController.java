package com.example.socialnetwork.controllers;

import com.example.socialnetwork.exceptions.DuplicateEntityException;
import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

    private UserService userService;
    private ImageService imageService;
    private PostService postService;
    private CommentService commentService;
    private UserRelationshipService userRelationshipService;

    @Autowired
    public UserController(UserService userService, ImageService imageService, PostService postService,
                          UserRelationshipService userRelationshipService) {
        this.userService = userService;
        this.imageService = imageService;
        this.postService = postService;
        this.userRelationshipService = userRelationshipService;
    }



    @GetMapping("/profile")
    public String getUser(Model model, Principal principal) {
        model.addAttribute("user", userService.getByUserName(principal.getName()));
        return "profile";
    }


    @GetMapping("/user/{userID}")
    public String showProfile(@PathVariable int userID, Model model) {

        try {
            model.addAttribute("user", userService.getUserById(userID));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d not found", userID));
        }

        return "profile";
    }

    @GetMapping("/update/{userID}")
    public String showUpdateForm(@PathVariable int userID, Model model) {
        model.addAttribute("user", userService.getUserById(userID));
        return "profile";
    }


    @PostMapping("/update/{userID}")
    public String updateProfile(@Valid @ModelAttribute("user") UserInformation user,
                                @PathVariable int userID,
                                @RequestParam("imageFile") MultipartFile file,
                                BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            user.setId(userID);
            return "user";
        }
        if (!file.isEmpty()) {
            imageService.saveImage(userID, file);
        }
        try {
            userService.updateUser(userID, user);
            model.addAttribute("users", userService.getAll());
        } catch (DuplicateEntityException ex) {
            model.addAttribute("error", ex);
            return "error";
        }
        return "redirect:/profile";
    }


    @GetMapping("/userlist")
    public String getUsers(Model model, @RequestParam(defaultValue = "0") int page) {
        model.addAttribute("users", userService.getAllPageable(PageRequest.of(page, 5)));
        model.addAttribute("currentPage", page);
        return "userlist";
    }

    @GetMapping("/result")
    public String showSearch(Model model, @RequestParam(defaultValue = "") String username,
                             @RequestParam(defaultValue = "0") int page) {

        model.addAttribute("users", userService.findAllByUserNameLike(username, PageRequest.of(page, 5)));
        model.addAttribute("currentPage", page);
        return "userlist";
    }

    @GetMapping("/userfeed")
    public String getUserFeed(Model model, Principal principal) {

        UserInformation user = userService.getCurrentUser(principal);
        model.addAttribute("user", userService.getByUserName(principal.getName()));
        model.addAttribute("posts", postService.postsCreatedBy(user));
        model.addAttribute("allposts", postService.getAll());
        model.addAttribute("postDTO", new PostDTO());
        model.addAttribute("comment", new Comment());
        model.addAttribute("friendlist",userRelationshipService.getAllFriends(user.getId()));
        model.addAttribute("friendrequests",userRelationshipService.requestForFriendShips(user.getId()));
        model.addAttribute("viral",currentViralPost());
        return "userfeed";
    }

    private Post currentViralPost(){
        List<Post> lastFewPosts = postService.getLatest3DaysPosts();
        int totalSum;
        int biggest =0;
        Post viral = new Post() ;
        for (int i = 0; i <lastFewPosts.size() ; i++) {
            totalSum=0;
            totalSum+=lastFewPosts.get(i).getLikes().size();
            totalSum+=lastFewPosts.get(i).getComments().size();
            if (totalSum>biggest){
                biggest=totalSum;
                viral = lastFewPosts.get(i);
            }
        }
        return viral;
    }

    @GetMapping("/request/accept/{requesteeID}")
    public String acceptRequest(@PathVariable int requesteeID, Principal principal){
        UserInformation user = userService.getByUserName(principal.getName());
        int receiverID = user.getId();
        userRelationshipService.answerFriendRequest(receiverID,requesteeID,"Approved");

        return "redirect:/userfeed";
    }

    @GetMapping("/request/reject/{requesteeID}")
    public String rejectRequest(@PathVariable int requesteeID, Principal principal){
        UserInformation user = userService.getByUserName(principal.getName());
        int receiverID = user.getId();
        userRelationshipService.answerFriendRequest(receiverID,requesteeID,"Reject");

        return "redirect:/userfeed";
    }


    @GetMapping("/profile/{userID}/userimage")
    public void renderUserPicture(@PathVariable int userID, HttpServletResponse response) throws IOException {
        UserInformation user = userService.getUserById(userID);

        if (user.getPicture() != null) {
            byte[] byteArray = new byte[user.getPicture().length];
            int i = 0;

            for (Byte wrappedByte : user.getPicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

}
