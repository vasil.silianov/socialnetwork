package com.example.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "user_relationship")
public class UserRelationship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int Id;

    @ManyToOne()
    @JoinColumn(name = "requester")
    private UserInformation requester;

    @ManyToOne()
    @JoinColumn(name = "requestee")
    private UserInformation requestee;

    @Column(name = "status")
    private String status;


    public UserRelationship() {
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public UserInformation getRequester() {
        return requester;
    }

    public void setRequester(UserInformation requester) {
        this.requester = requester;
    }

    public UserInformation getRequestee() {
        return requestee;
    }

    public void setRequestee(UserInformation requestee) {
        this.requestee = requestee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


