package com.example.socialnetwork.exceptions;

public class CommentDoesNotExistsException extends RuntimeException {
    public CommentDoesNotExistsException(String msg) {
        super(msg);
    }
}
