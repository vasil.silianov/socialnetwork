package com.example.socialnetwork.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int postId;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInformation user;

    @Size(min = 2, max = 150, message = "You need to write something")
    @Column(name = "post_text")
    private String postText;

    @Column(name = "enabled")
    private int enabled;

    @Column(name = "date_created")
    private Date createdDate;

    @Column (name="public")
    private int visibility;

    @JsonIgnore
    @Lob
    @Column(name = "image")
    private Byte[] image;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "post_id")
    @Where(clause = "enabled = 1")
    private List<Comment> comments;//maybe change to List

    @ManyToMany
    @JoinTable(
            name = "likes",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<UserInformation> likes;

    public Post() {
    }


    public Post(int postId, UserInformation user, String postText) {
        this.postId = postId;
        this.user = user;
        this.postText = postText;
    }

    public Byte[] getImage() {
        return image;
    }

    public Set<UserInformation> getLikes() {
        return likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public int getVisibility() {
        return visibility;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public int getEnabled() {
        return enabled;
    }

    public int getPostId() {
        return postId;
    }

    public UserInformation getUser() {
        return user;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public void setUser(UserInformation user) {
        this.user = user;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setLikes(Set<UserInformation> likes) {
        this.likes = likes;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }
}
