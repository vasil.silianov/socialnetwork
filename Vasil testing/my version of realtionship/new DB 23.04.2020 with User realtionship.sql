-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.67-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for social_network
DROP DATABASE IF EXISTS `social_network`;
CREATE DATABASE IF NOT EXISTS `social_network` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `social_network`;

-- Dumping structure for table social_network.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `username_authority` (`username`,`authority`),
  CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.authorities: ~9 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('dionisiev', 'ROLE_USER'),
	('Eti', 'ROLE_USER'),
	('gekul', 'ROLE_USER'),
	('gogo', 'ROLE_ADMIN'),
	('gogo', 'ROLE_USER'),
	('iliq', 'ROLE_USER'),
	('mimi', 'ROLE_USER'),
	('Tedi', 'ROLE_USER'),
	('vas', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table social_network.comments
DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment_text` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`comment_id`),
  KEY `comments_posts_post_id_fk` (`post_id`),
  KEY `comments_users_user_id_fk` (`user_id`),
  CONSTRAINT `comments_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `comments_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.comments: ~1 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `post_id`, `user_id`, `comment_text`, `date_created`, `enabled`) VALUES
	(1, 2, 5, 'Nice post', '2020-04-12 18:09:33', 1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table social_network.likes
DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `likes_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`likes_id`),
  KEY `likes_posts_post_id_fk` (`post_id`),
  KEY `likes_users_user_id_fk` (`user_id`),
  CONSTRAINT `likes_posts_post_id_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  CONSTRAINT `likes_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;

-- Dumping structure for table social_network.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_text` text,
  `enabled` tinyint(4) DEFAULT '1',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `public` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`post_id`),
  KEY `posts_users_user_id_fk` (`user_id`),
  CONSTRAINT `posts_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_information` (`user_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.posts: ~2 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `user_id`, `post_text`, `enabled`, `date_created`, `public`) VALUES
	(2, 5, 'First post', 1, '2020-04-11 13:05:42', 1),
	(3, 5, 'Second post', 1, '2020-04-11 16:06:47', 1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table social_network.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.users: ~8 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('dionisiev', '$2a$10$BFHze2O7pMkOywrzkLANIurAhANl3xhocwOZ2cclxnrflRpUthy2C', 1),
	('Eti', '$2a$10$z1DQfFBYC/praQwL9JyNBezaX9ZHoqKK8KXVWyVfqiQXof/N2hfmK', 1),
	('gekul', '1234', 1),
	('gogo', '$2a$10$wP4mQw9BLb45/M2zieUsJerCJ4Tn1ol84.6NSbEvDBWgO.u7oVuQu', 1),
	('iliq', '$2a$10$52LGMTvbxgB3Rx/zFPLH/.zrJ5PIv.et0Xc8l6kYf0uKCOHsRVPDO', 1),
	('mimi', '$2a$10$uW5GW/WWEOsOD7v7UPXUCOqDoygoKdEdxsbheOfkILturxpdAlc92', 1),
	('Tedi', '$2a$10$Ot2cR8ZVybj7NOqWKPxnDOdio4twCON2NUCS7GY1qr3y89vhB6V.O', 1),
	('vas', '$2a$10$H5Fu1WINyesTavJrYSxQoOHA9igDLHgr5U2vcvnD1X3kyoEnCChrC', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table social_network.users_information
DROP TABLE IF EXISTS `users_information`;
CREATE TABLE IF NOT EXISTS `users_information` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(68) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  `picture` mediumblob,
  PRIMARY KEY (`user_id`),
  KEY `users_old_users_username_fk` (`username`),
  CONSTRAINT `users_old_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.users_information: ~8 rows (approximately)
/*!40000 ALTER TABLE `users_information` DISABLE KEYS */;
INSERT INTO `users_information` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `enabled`, `picture`) VALUES
	(5, 'Georgi', 'Kulov', 'gekul', '1234', 'kulov@suck.it', 1, NULL),
	(23, NULL, NULL, 'gogo', 'gogo', NULL, 1, NULL),
	(24, NULL, NULL, 'mimi', 'mimi', NULL, 1, NULL),
	(25, 'Ekaterina', 'moma', 'Eti', '12345', 'eti.sexa@gmail.com', 1, NULL),
	(26, 'Teodora', 'moma', 'Tedi', '12345', 'tedi.sexa@gmail.com', 1, NULL),
	(27, NULL, NULL, 'vas', '123', NULL, 1, NULL),
	(28, NULL, NULL, 'iliq', 'qwerty', NULL, 1, NULL),
	(29, NULL, NULL, 'dionisiev', '1234', NULL, 1, NULL);
/*!40000 ALTER TABLE `users_information` ENABLE KEYS */;

-- Dumping structure for table social_network.user_relationship
DROP TABLE IF EXISTS `user_relationship`;
CREATE TABLE IF NOT EXISTS `user_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` varchar(29) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_relationship_pk_2` (`user_one_id`,`user_two_id`),
  KEY `user_relationship_users_information_user_id_fk_2` (`user_two_id`),
  CONSTRAINT `user_relationship_users_information_user_id_fk` FOREIGN KEY (`user_one_id`) REFERENCES `users_information` (`user_id`),
  CONSTRAINT `user_relationship_users_information_user_id_fk_2` FOREIGN KEY (`user_two_id`) REFERENCES `users_information` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table social_network.user_relationship: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_relationship` DISABLE KEYS */;
INSERT INTO `user_relationship` (`id`, `user_one_id`, `user_two_id`, `status`) VALUES
	(1, 25, 26, 'approved'),
	(2, 25, 27, 'approved'),
	(3, 28, 25, 'approved'),
	(4, 29, 25, 'not');
/*!40000 ALTER TABLE `user_relationship` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
