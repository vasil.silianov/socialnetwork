package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.UserRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRelationshipRepository extends JpaRepository<UserRelationship, Integer> {

    @Query("SELECT r FROM UserRelationship r WHERE r.userOne.id = :id")
    List<UserRelationship> filterUserOne(@Param("id") int id); // i taka ne  e zashtoto mi trqbva drugata kolona vsushtnost

    @Query("SELECT r FROM UserRelationship r WHERE r.userTwo.id = :id")
    List<UserRelationship> filterUserTwo(@Param("id") int id);

    UserRelationship getUserRelationshipByUserOneAndUserTwo(int userOne, int userTwo);

    UserRelationship findUserRelationshipByUserOneIdAndUserTwoId(int userOne, int userTwo);
}
