package com.example.socialnetwork.services.contracts;


import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.DTO.CommentDTO;
import com.example.socialnetwork.models.UserInformation;

public interface CommentService {

    Comment createComment(CommentDTO commentDTO, UserInformation user);

    Comment getComment(int commentId);

    Comment updateComment(CommentDTO commentDTO, UserInformation user);

    Comment deleteComment(CommentDTO commentDTO, UserInformation user);
}
