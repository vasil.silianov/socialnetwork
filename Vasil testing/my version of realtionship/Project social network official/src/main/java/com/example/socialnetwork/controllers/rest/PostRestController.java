package com.example.socialnetwork.controllers.rest;


import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.services.contracts.UserService;
import com.example.socialnetwork.services.contracts.PostService;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;
    private UserService userService;

    @Autowired
    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/all")
    public List<Post> getAllPosts() {
        try {
            return postService.getAll();
        } catch (PostDoesNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/dto")
    public List<PostDTO> getAllPostsDto() {
        try {
            return postService.getAllDto();
        } catch (PostDoesNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{postID}")
    public Post getPostById(@PathVariable int postID){
        try{
            return postService.getPostById(postID);
        }catch (PostDoesNotExistsException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //not working
    @PostMapping("/new")
    public Post createPost(@Valid @RequestBody PostDTO post, Principal principal){

        return postService.createPost(post,principal.getName());

    }

    @DeleteMapping("/{postID}")
    private void deletePostByID(@PathVariable int postID) {

        try{
            postService.deletePostByID(postID);

        }catch (PostDoesNotExistsException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


}
