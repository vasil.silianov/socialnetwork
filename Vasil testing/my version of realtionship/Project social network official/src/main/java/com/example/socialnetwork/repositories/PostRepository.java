package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import javafx.geometry.Pos;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> findAllByUserUserName(String username);


  //  List<Post> postsCreatedBy(int id);

    List<Post> findAllByUserOrderByCreatedDateDesc(UserInformation user);

    //    Optional<Post> getAll();
//
//    Optional<Post> getByID(int id);
}
