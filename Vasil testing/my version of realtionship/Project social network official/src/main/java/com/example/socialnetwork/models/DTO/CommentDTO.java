package com.example.socialnetwork.models.DTO;

public class CommentDTO {

    private int postId;

    private int userId;

    private String content;

    public CommentDTO() {
    }

    public int getPostId() {
        return postId;
    }

    public int getUserId() {
        return userId;
    }

    public String getContent() {
        return content;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
