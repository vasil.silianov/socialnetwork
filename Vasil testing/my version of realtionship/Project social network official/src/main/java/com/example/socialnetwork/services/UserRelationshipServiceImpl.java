package com.example.socialnetwork.services;

import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.models.UserRelationship;
import com.example.socialnetwork.repositories.UserRelationshipRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.UserRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRelationshipServiceImpl implements UserRelationshipService {

    private UserRelationshipRepository userRelationshipRepository;
    private UserRepository userRepository;

    @Autowired
    public UserRelationshipServiceImpl(UserRelationshipRepository userRelationshipRepository, UserRepository userRepository) {
        this.userRelationshipRepository = userRelationshipRepository;
        this.userRepository = userRepository;
    }


    public List<UserInformation> getAllFriends(int currentUser) {
        List<UserInformation> friendList = new ArrayList<>();
        List<UserRelationship> relationships = userRelationshipRepository.filterUserOne(currentUser);
        List<UserRelationship> relationships2 = userRelationshipRepository.filterUserTwo(currentUser);
        //todo  multy thredead
        for (int i = 0; i < relationships.size(); i++) {

            if (relationships.get(i).getStatus().equals("approved")) {
                friendList.add(relationships.get(i).getUserTwo());
            }

        }
        for (int i = 0; i < relationships2.size(); i++) {
            if (relationships2.get(i).getStatus().equals("approved")) {
                friendList.add(relationships2.get(i).getUserOne());
            }
        }
        return friendList;

    }


    /**
     *
     * @param currentUser
     * @param userToFriend
     */
    public void sendFriendRequest(int currentUser, int userToFriend) {

        UserRelationship newRealationsheeeeep = new UserRelationship();
        if (currentUser > userToFriend) {
            newRealationsheeeeep.setUserOne(userRepository.getOne(userToFriend));
            newRealationsheeeeep.setUserTwo(userRepository.getOne(currentUser));
            newRealationsheeeeep.setStatus("Pending_0");//UserOne is must answer request
        } else if (currentUser < userToFriend) {
            newRealationsheeeeep.setUserTwo(userRepository.getOne(userToFriend));
            newRealationsheeeeep.setUserOne(userRepository.getOne(currentUser));
            newRealationsheeeeep.setStatus("Pending_1");//UserTwo is must answer request
        } else {
            throw new IllegalArgumentException("Users can not friend them self");
        }

        userRelationshipRepository.save(newRealationsheeeeep);

    }


    public void answerFriendRequest(int current,int sender, String answer) {
//        UserRelationship userRelationship = userRelationshipRepository.getUserRelationshipByUserOneAndUserTwo(sender,current);
        UserRelationship userRelationship = userRelationshipRepository.findUserRelationshipByUserOneIdAndUserTwoId(sender,current);
        System.out.println(userRelationship.getId() + " " + userRelationship.getUserOne().getUserName()  + " " + userRelationship.getUserTwo().getUserName() + " " + userRelationship.getStatus());
        userRelationship.setStatus(answer);
        userRelationshipRepository.save(userRelationship);//todo to chek if the person who answer the request is  the correct user
    }


    public List<UserInformation> requestForFriendShips(int currentUser) {
        List<UserInformation> request = new ArrayList<>();
        List<UserRelationship> relationships = userRelationshipRepository.filterUserOne(currentUser);
        List<UserRelationship> relationships2 = userRelationshipRepository.filterUserTwo(currentUser);
 ///todo multi threaded
        for (int i = 0; i < relationships.size(); i++) {
            if (relationships.get(i).getStatus().equals("Pending_0")){
            request.add(relationships.get(i).getUserTwo());
            }
        }

        for (int i = 0; i < relationships2.size(); i++) {
            if (relationships2.get(i).getStatus().equals("Pending_1")){
            request.add(relationships2.get(i).getUserOne()); // ne copy  pastvai  bezmozychno tova ti ebava maikata
            }
        }
        return request;
    }



}
