package com.example.socialnetwork.services.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;

public interface ImageService {
    void saveImage(int id, MultipartFile file);
}
