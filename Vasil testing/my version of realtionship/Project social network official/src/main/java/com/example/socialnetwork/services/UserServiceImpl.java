package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.NoEntityFoundException;
import com.example.socialnetwork.models.UserDAO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserDAORepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserDAORepository userDAORepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserDAORepository userDAORepository) {
        this.userRepository = userRepository;
        this.userDAORepository = userDAORepository;
    }


    @Override
    public Collection<UserInformation> getAll() {
        return userRepository.findAll();
    }

    @Override
    public void createUser(UserInformation user) {
        userRepository.save(user);
    }

    @Override
    public UserInformation getUserById(int userID) {
        Optional<UserInformation> user = userRepository.findById(userID);
        if(!user.isPresent()){
            throw new EntityNotFoundException("user",userID);
        }
       return user.get();
    }

    @Override
    public UserInformation getCurrentUser(Principal principal) {
        return getByUsername(principal.getName());
    }

    @Override
    public UserInformation getByUsername(String username) {
        return userRepository.getByUserName(username)
                .orElseThrow(() -> new NoEntityFoundException
                          (String.format("User with username: %s not found", username)));
    }

    @Override
    public String disableUser(int id) {
        String name = userRepository.getOne(id).getUserName();
        UserDAO userToBeDisabled = userDAORepository.getOne(name);
        userToBeDisabled.setEnabled(0);// tova moje da se napravi na edin red mai
        userDAORepository.save(userToBeDisabled);// tova go zabravih
        return "user " + name + " was disabled";
    }


    @Override
    public UserInformation getByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public UserInformation updateUser(int id, UserInformation newUser) {

        UserInformation user = getUserById(id);

        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setEmail(newUser.getEmail());
        return userRepository.save(user);
    }


//    private User changeSecurityUserEnabledStatus(UserDetails userDetails) {
//        User user = (User) userDetailsManager.loadUserByUsername(userDetails.getEmail());
//        return new User(user.getUsername(), user.getPassword(), !user.isEnabled(), true, true, true, user.getAuthorities());
//    }
}

