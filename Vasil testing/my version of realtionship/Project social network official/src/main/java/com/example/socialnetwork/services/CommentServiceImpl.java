package com.example.socialnetwork.services;

import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.DTO.CommentDTO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.CommentRepository;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;
    private PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }


    @Override
    public Comment createComment(CommentDTO commentDTO, UserInformation user) {

        return null;
    }

    @Override
    public Comment getComment(int commentId) {
        Optional<Comment> comment = commentRepository.findById(commentId);
        return comment.get();
    }

    @Override
    public Comment updateComment(CommentDTO commentDTO, UserInformation user) {
        return null;
    }

    @Override
    public Comment deleteComment(CommentDTO commentDTO, UserInformation user) {
        return null;
    }
}
