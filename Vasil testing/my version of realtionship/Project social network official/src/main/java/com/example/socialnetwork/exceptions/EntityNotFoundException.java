package com.example.socialnetwork.exceptions;


public  class EntityNotFoundException extends RuntimeException{
public  EntityNotFoundException(String type, int attribute) {
        super(String.format("%s with %s doesn't exists.",type,attribute));
    }
}