package com.example.socialnetwork.controllers.rest;

import com.example.socialnetwork.models.DTO.UserDTO;
import com.example.socialnetwork.models.UserDAO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserDAORepository;
import com.example.socialnetwork.services.contracts.UserRelationshipService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private PasswordEncoder passwordEncoder;
    private UserDAORepository userDAORepository;
    private UserRelationshipService userRelationshipService;

    @Autowired
    public UserRestController(UserService userService, PasswordEncoder passwordEncoder, UserDetailsManager userDetailsManager,
                              UserDAORepository userDAORepository, UserRelationshipService userRelationshipService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.userDAORepository = userDAORepository;
        this.userRelationshipService = userRelationshipService;
    }


    @GetMapping
    public Collection<UserInformation> getAll() {
        return userService.getAll();
    }


    @GetMapping("/current")
    public UserInformation getCurrentUser(Authentication principal) {
        return userService.getCurrentUser(principal);
    }

    @GetMapping("/{username}")
    public UserInformation getUserByName(@PathVariable String username) {
        return userService.getByUsername(username);
    }

    @PutMapping("/disable/{id}")
    public String disableUser(@PathVariable int id) {
        return userService.disableUser(id);
    }


    @PostMapping("/creat")
    public String creatUser(@RequestBody UserDTO userDTO) {// koq logika kyde da sedi
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser = new org.springframework.security.core.userdetails
                .User(userDTO.getUserName(),
                passwordEncoder.encode(userDTO.getPassword()), authorities);

        UserInformation userDetails = new UserInformation();
        userDetails.setEmail(userDTO.getEmail());
        userDetails.setUserName(userDTO.getUserName());
        userDetails.setFirstName(userDTO.getFirstName());
        userDetails.setPassword(userDTO.getPassword());
        userDetails.setLastName(userDTO.getLastName());


        try {
            userDetailsManager.createUser(newUser);
            userService.createUser(userDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return "nice";
    }


    @PutMapping("/update/{username}")
    public UserInformation editUser(@RequestBody UserDTO userDTO, @PathVariable String username) {

        UserDAO userToUpdateSecurity = userDAORepository.getOne(userDTO.getUserName());

        userToUpdateSecurity.setUsername(userDTO.getUserName());
        userToUpdateSecurity.setPassword(userDTO.getPassword());

        UserInformation userToUpdate = userService.getByUserName(username);

        userToUpdate.setEmail(userDTO.getEmail());
        userToUpdate.setUserName(userDTO.getUserName());
        userToUpdate.setFirstName(userDTO.getFirstName());
        userToUpdate.setPassword(userDTO.getPassword());
        userToUpdate.setLastName(userDTO.getLastName());

        try {
            userDAORepository.save(userToUpdateSecurity);
            userService.createUser(userToUpdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return responseDtoManager.transformUserToResponse(editManager.editProfile(userDto, username), principal);
        return userToUpdate;
    }

    @GetMapping("/friendlist")
    public List<UserInformation> friendList(@RequestHeader(name = "Authorization") String principal) {
        return userRelationshipService.getAllFriends(getUserByName(principal).getId());
    }

    @PostMapping("/sendfrrequest/{userToFriend}")
    public String sendFriendRequest(@RequestHeader(name = "Authorization") String principal,@PathVariable String userToFriend){
        UserInformation currentUser = getUserByName(principal);
        UserInformation userToBeFriend = getUserByName(userToFriend);
        userRelationshipService.sendFriendRequest(currentUser.getId(),userToBeFriend.getId());
        return String.format("User %s send friend request to user %s",currentUser.getUserName(),userToBeFriend.getUserName());
    }

    @PutMapping("/response/{userToFriend}/{answer}")
    public String answerRequest(@RequestHeader(name = "Authorization") String principal,@PathVariable String userToFriend, @PathVariable String answer){

        UserInformation currentUser = userService.getByUserName(principal);
        UserInformation sender = userService.getByUserName(userToFriend);

        userRelationshipService.answerFriendRequest(currentUser.getId(),sender.getId(),answer);
        return  String.format("User %s responded to user %s's request with: %s" ,principal, userToFriend,answer);
    }

    @GetMapping("/friendrequests")
    public List<UserInformation> requestsForFriendships(@RequestHeader(name = "Authorization") String principal){
        return userRelationshipService.requestForFriendShips(userService.getByUserName(principal).getId());

    }
}

