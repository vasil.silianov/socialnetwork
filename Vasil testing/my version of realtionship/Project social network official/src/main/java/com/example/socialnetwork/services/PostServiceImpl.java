package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    public static final String POST_NOT_FOUND = "There isn't any post with ID %d";


    private PostRepository postRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }



    @Override
    public List <Post> getAll() {
        return postRepository.findAll();
    }

    @Override
    public List<PostDTO> getAllDto() {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(post -> modelMapper.map(post, PostDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Post getPostById(int postID) {
        Optional<Post> post = postRepository.findById(postID);
        if(!post.isPresent()){
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND,postID));
        }
        return post.get();
    }

    @Override
    public Post createPost(PostDTO postDTO, String username) {
        UserInformation user = userRepository.findByUserName(username);

        Post post = new Post();
        post.setUser(user);
        post.setVisibility(1);
        post.setCreatedDate(new Date());
        return postRepository.save(post);
    }

    @Override
    public void deletePostByID(int postID) {
        Optional<Post> newPost = postRepository.findById(postID);
        if(!newPost.isPresent()){
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND,postID));
        }
         postRepository.deleteById(postID);
    }

    @Override
    public List<Post> getPostByUsername(String username) {

     return postRepository.findAllByUserUserName(username);

    }

    @Override
    public List<Post> postsCreatedBy(UserInformation user) {
        return postRepository.findAllByUserOrderByCreatedDateDesc(user);
    }

    public PostDTO changeToDTO(Post post) {
        PostDTO postDTO = modelMapper.map(post, PostDTO.class);
        return postDTO;
    }




}
