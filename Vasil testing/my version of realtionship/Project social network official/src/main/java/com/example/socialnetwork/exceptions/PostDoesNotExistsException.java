package com.example.socialnetwork.exceptions;

public class PostDoesNotExistsException extends RuntimeException {
    public PostDoesNotExistsException(String message) {
        super(message);
    }
}