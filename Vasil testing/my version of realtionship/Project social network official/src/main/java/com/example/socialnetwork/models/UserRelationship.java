package com.example.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "user_relationship")
public class UserRelationship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int Id;

    @ManyToOne()
    @JoinColumn(name = "user_one_id")
    private UserInformation userOne;

    @ManyToOne()
    @JoinColumn(name = "user_two_id")
    private UserInformation userTwo;

    @Column(name = "status")
    private String status;


    public UserRelationship() {
    }


    public int getId() {
        return Id;
    }

    public UserInformation getUserOne() {
        return userOne;
    }

    public void setUserOne(UserInformation userOne) {
        this.userOne = userOne;
    }

    public UserInformation getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(UserInformation userTwo) {
        this.userTwo = userTwo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
