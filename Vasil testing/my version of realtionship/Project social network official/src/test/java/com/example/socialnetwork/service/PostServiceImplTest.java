package com.example.socialnetwork.service;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.repositories.PostRepository;

import com.example.socialnetwork.services.PostServiceImpl;
import org.junit.Assert;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

    @Mock
    PostRepository mockRepository;

    @InjectMocks
    PostServiceImpl service;


    @Test
    public void getAllPosts_Should_ReturnPosts() {
        //Arrange

        service.getAll();

        verify(mockRepository, times(1));

    }

//    @Test
//    public void getPostById_Should_CallRepository() {
//        Post post  = new Post();
//        post.setPostId(2);
//
//        Mockito.when(mockRepository.getByID(2)).thenReturn(Optional.of(post));
//
//        //Act
//        Post result = service.getPostById(2);
//
//        //Assert
//        Assert.assertEquals(2,result.getPostId());
//    }

}
