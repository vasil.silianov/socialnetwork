package com.example.socialnetwork.controllers;

import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile")
    public String getUser(Model model, Principal principal){
        model.addAttribute("user", userService.getByUserName(principal.getName()));
        return "profile";
    }
}
