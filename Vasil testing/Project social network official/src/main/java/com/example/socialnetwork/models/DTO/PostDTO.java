package com.example.socialnetwork.models.DTO;

import com.example.socialnetwork.models.UserInformation;
import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

public class PostDTO {


    @NotNull
    @Size(min = 2, max = 150, message = "You need to write something")
    private String postText;

    private int visibility;

    private UserInformation user;


    public PostDTO() {
    }

    public int getVisibility() {
        return visibility;
    }

    public UserInformation getUser() {
        return user;
    }

    public String getPostText() {
        return postText;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public void setUser(UserInformation user) {
        this.user = user;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

}
