package com.example.socialnetwork.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class HibernateConfig {

    private String dbURl, dbUsername, dBPassword;

    @Autowired
    public HibernateConfig(Environment env) {
        dbURl = env.getProperty("spring.database.url");
        dbUsername = env.getProperty("spring.database.username");
        dBPassword = env.getProperty("spring.database.password");
    }

//    @Bean
//    public LocalSessionFactoryBean sessionFactory() {
//        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
//        sessionFactory.setDataSource(dataSource());
//        sessionFactory.setPackagesToScan("com.example.socialnetwork.models");
//        sessionFactory.setHibernateProperties(hibernateProperties());
//        return sessionFactory;
//    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(dbURl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dBPassword);
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create-drop");

        return hibernateProperties;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.example.socialnetwork.models");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(hibernateProperties());
        return em;
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    //    @Bean
    //    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    //
    //        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    //        vendorAdapter.setGenerateDdl(true);
    //        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    //        factory.setJpaVendorAdapter(vendorAdapter);
    //        factory.setPackagesToScan("com.addonis.demo.models");
    //        factory.setDataSource(dataSource());
    //        factory.afterPropertiesSet();
    //        return factory;
    //    }
    //
    //    @Bean
    //    public DataSource dataSource() {
    //        DriverManagerDataSource dataSource = new DriverManagerDataSource();
    //        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    //        dataSource.setUrl(dbUrl);
    //        dataSource.setUsername(dbUsername);
    //        dataSource.setPassword(dbPassword);
    //        return dataSource;
    //    }
    //
    //    @Bean
    //    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
    //
    //        JpaTransactionManager txManager = new JpaTransactionManager();
    //        txManager.setEntityManagerFactory(entityManagerFactory);
    //        return txManager;
    //    }
}

