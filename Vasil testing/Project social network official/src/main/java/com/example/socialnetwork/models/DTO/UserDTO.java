package com.example.socialnetwork.models.DTO;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserDTO {


    @NotNull
    @NotEmpty(message = "Username cannot be empty")
    @Size(min = 2, max = 25, message = "First name must be between 2 and 25 symbols")
    private String userName;

    @NotNull
    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 2, max = 25, message = "Last name must be between 2 and 25 symbols")
    private String password;

    //@NotNull
    private String firstName;

    //@NotNull
    private String lastName;

    //@NotBlank
    private String email;

    private String passwordConfirmation;

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
