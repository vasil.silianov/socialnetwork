package com.example.socialnetwork.controllers.rest;

import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentRestController {

    private CommentService commentService;

    @Autowired
    public CommentRestController(CommentService commentService) {
        this.commentService = commentService;
    }



}
