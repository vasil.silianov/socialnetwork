package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserInformation, Integer> {


    Optional<UserInformation> getByuserName(String name);
    UserInformation findByUserName(String username);
//    List<User> filterByUserName(String username); //moje bi raboti da se testva
}
