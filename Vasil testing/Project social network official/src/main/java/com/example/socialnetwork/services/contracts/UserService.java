package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.UserInformation;

import java.security.Principal;
import java.util.Collection;

public interface UserService {
    Collection<UserInformation> getAll();

    void createUser(UserInformation user);

    void getUserById(int userID);
    UserInformation getCurrentUser(Principal principal);
    UserInformation getByUsername(String username);
    String disableUser( int id);



    UserInformation getByUserName(String username);



}
