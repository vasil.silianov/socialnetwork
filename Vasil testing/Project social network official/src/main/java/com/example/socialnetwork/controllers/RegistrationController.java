package com.example.socialnetwork.controllers;



import com.example.socialnetwork.models.DTO.UserDTO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    private UserService userService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserService userService, UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model){
        model.addAttribute("user", new UserDTO());
        return "login-register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDTO userDTO,
                               BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            model.addAttribute("error","Username/password can't be empty!");
            return "login-register";
        }

        if (userDetailsManager.userExists(userDTO.getUserName())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "login-register";
        }
        System.out.println(userDTO.getPassword());

        if (!userDTO.getPassword().equals(userDTO.getPasswordConfirmation())) {
            model.addAttribute("error", "Password doesn't much!");
            return "login-register";
        }


        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser = new org.springframework.security.core.userdetails
                .User(userDTO.getUserName(),
                passwordEncoder.encode(userDTO.getPassword()), authorities);

        UserInformation userDetails = new UserInformation();
        userDetails.setEmail(userDTO.getEmail());
        userDetails.setUserName(userDTO.getUserName());
        userDetails.setFirstName(userDTO.getFirstName());
        userDetails.setPassword(userDTO.getPassword());
        userDetails.setLastName(userDTO.getLastName());


        try {
            userDetailsManager.createUser(newUser);
            userService.createUser(userDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "nice";
    }



    @GetMapping("/register-confirmation")
    public  String showRegisterConfirmation(){
        return "register-confirmation";
    }

//    private User changeSecurityUserEnabledStatus(UserDetails userDetails) {
//
//        User user = (User) userDetailsManager.loadUserByUsername(userDetails.getEmail());
//
//        return new User(user.getUsername(),
//                user.getPassword(),
//                !user.isEnabled(),
//                true, true, true,
//                user.getAuthorities());
//
//    }


}
