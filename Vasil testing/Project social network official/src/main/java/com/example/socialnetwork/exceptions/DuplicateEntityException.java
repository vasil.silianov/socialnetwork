package com.example.socialnetwork.exceptions;

public class DuplicateEntityException extends RuntimeException{
    public DuplicateEntityException(String type, String attribute){
        super(String.format("%s with %s already exists.",type,attribute));
    }
}
