package com.example.socialnetwork.controllers;

import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.services.contracts.PostService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class PostController {
    private UserService userService;
    private PostService postService;

    @Autowired
    public PostController(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @PostMapping("/posts")
    @RequestMapping
    public String createPost(@Valid @ModelAttribute PostDTO postDTO, BindingResult bindingResult,
                             Model model, Principal principal){
        //Validation

//        postService.createPost(postDTO, userService.getCurrentUser(principal.getName()));

        return null;
    }
}
