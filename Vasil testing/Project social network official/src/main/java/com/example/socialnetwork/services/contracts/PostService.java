package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;

import java.util.List;

public interface PostService {
    List<Post> getAll();

    List<PostDTO> getAllDto();

    Post getPostById(int postID);

     Post createPost(PostDTO postDTO, String username);

     void deletePostByID(int postID);

}
