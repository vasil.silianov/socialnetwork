package com.example.socialnetwork.exceptions;

public class NoEntityFoundException extends IllegalArgumentException {
    public NoEntityFoundException(String message) {
        super(message);
    }
}
