package com.example.socialnetwork.service;

import com.example.socialnetwork.exceptions.NoEntityFoundException;
import com.example.socialnetwork.models.UserDAO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserDAORepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @InjectMocks
    UserServiceImpl mockUserService;

    @Mock
    UserRepository userRepository;

    @Mock
    UserDAORepository userDAORepository;


    @Test
    public  void getUserByIdShould_CallRepository(){
        //arrange
        UserInformation user  = new UserInformation();
        user.setId(1);

        Mockito.when(userRepository.getOne(1))
                .thenReturn(user);
        //act
        mockUserService.getUserById(1);

        //assert
        Mockito.verify(userRepository,Mockito.times(1)).getOne(1);
    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists(){
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("gari");
        Mockito.when(userRepository.getByuserName(Mockito.anyString()))
                .thenReturn(java.util.Optional.of(user));
        // act
      UserInformation userToReturn = mockUserService.getByUsername("vasko");
        //assert
        Assert.assertEquals(user.getUserName(),userToReturn.getUserName());
    }

    @Test(expected = NoEntityFoundException.class)
    public void getUserByUserNameShould_ThrowNoEntityFoundException_WhenUserDoesNotExists(){
        //arrange,act,assert
        mockUserService.getByUsername("Vasil");

    }

    @Test
    public  void disableUserShould_DisableUser(){
        //arrange
        UserDAO userDAO = new UserDAO();
        UserInformation user = new UserInformation();
        userDAO.setUsername("svetlio");
        user.setUserName("svetlio");
        String name = "svetlio";

        Mockito.when(userRepository.getOne(Mockito.anyInt()))
                .thenReturn(user);
        Mockito.when(userDAORepository.getOne(Mockito.anyString()))
                .thenReturn(userDAO);
        String expectedMessage = "user svetlio was disabled";

        //act
        String returndMessage = mockUserService.disableUser(1);

        //asser
        Assert.assertEquals(expectedMessage,returndMessage);
    }

    @Test
    public  void disableUserShould_CallRepository(){
        //arrange
        UserDAO userDAO = new UserDAO();
        UserInformation user = new UserInformation();
        userDAO.setUsername("svetlio");
        user.setUserName("svetlio");
        String name = "svetlio";

        Mockito.when(userRepository.getOne(Mockito.anyInt()))
                .thenReturn(user);
        Mockito.when(userDAORepository.getOne(Mockito.anyString()))
                .thenReturn(userDAO);
        //act
         mockUserService.disableUser(1);
        //asser
        Mockito.verify(userDAORepository,Mockito.times(1)).save(userDAO);
    }


    @Test
    public void creatUserShould_CreatUser(){
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("vaslp");
        user.setPassword("asd");
        user.setFirstName("vas");
        user.setLastName("sil");

        //act
        mockUserService.createUser(user);

        //assert
        Mockito.verify(userRepository,Mockito.times(1)).save(user);
    }

    @Test
    public  void getAllShould_ReturnAllUsersInRepository(){
        //arrange
        UserInformation user1 = new UserInformation();
        UserInformation user2 = new UserInformation();
        UserInformation user3 = new UserInformation();
        user2.setUserName("pesho");
        user3.setUserName("toshko");
        user1.setUserName("Nadya");

        List<UserInformation> testList = new LinkedList<>();
        testList.add(user1);
        testList.add(user2);
        testList.add(user3);

        Mockito.when(userRepository.findAll())
                .thenReturn(testList);
        // act

        //assert
        Assert.assertEquals(testList, mockUserService.getAll());

    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists2(){
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("gari");
        Mockito.when(userRepository.findByUserName(Mockito.anyString()))
                .thenReturn(user);
        // act
        UserInformation userToReturn = mockUserService.getByUserName("gari");
        //assert
        Assert.assertEquals(user.getUserName(),userToReturn.getUserName());
    }

}
