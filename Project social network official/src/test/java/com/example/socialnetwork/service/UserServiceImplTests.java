package com.example.socialnetwork.service;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @InjectMocks
    UserServiceImpl mockUserService;

    @Mock
    UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private Principal principal;

    @Mock
    UserDetailsManager userDetailsManager;
//    UserDAORepository userDAORepository;

    @Test
    public void getUserByIdShould_CallRepository() {
        //arrange
        UserInformation user = new UserInformation();
        user.setId(1);
        Mockito.when(userRepository.findById(1))
                .thenReturn(java.util.Optional.of(user));
        //act
        Assert.assertNotNull(mockUserService.getUserById(1));
        //assert
        Mockito.verify(userRepository, Mockito.times(1)).findById(1);
    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists() {
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("gari");
        Mockito.when(userRepository.getByUserName(Mockito.anyString()))
                .thenReturn(java.util.Optional.of(user));
        // act
        UserInformation userToReturn = mockUserService.getByUsername("vasko");
        //assert
        Assert.assertEquals(user.getUserName(), userToReturn.getUserName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByUserNameShould_ThrowNoEntityFoundException_WhenUserDoesNotExists() {
        //arrange,act,assert
        mockUserService.getByUsername("Vasil");

    }

    @Test
    public void disableUserShould_DisableUser() {
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("svetlio");
        user.setPassword("asd");
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn("asd");
        org.springframework.security.core.userdetails.User userDAO = new org.springframework.security.core.userdetails.User(user.getUserName(),
                (user.getPassword()), false,
                true, true, true, authorities);

        Mockito.when(userRepository.getOne(Mockito.anyInt()))
                .thenReturn(user);
        Mockito.when(userDetailsManager.loadUserByUsername(Mockito.anyString()))
                .thenReturn(userDAO);
        String expectedMessage = "user svetlio was disabled";
        //act
        String returnedMessage = mockUserService.disableUser(1);
        //asser
        Assert.assertEquals(expectedMessage, returnedMessage);
    }

    @Test
    public void disableUserShould_CallRepository() {
        UserInformation user = new UserInformation();
        user.setId(1);
        user.setUserName("svetlio");
        user.setPassword("123");
        user.setEnabled(1);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn("123");
        org.springframework.security.core.userdetails.User userDAO = new org.springframework.security.core.userdetails.User(user.getUserName(),
                (user.getPassword()), false,
                true, true, true, authorities);
        Mockito.when(userRepository.getOne(1))
                .thenReturn(user);
        Mockito.when(userDetailsManager.loadUserByUsername(Mockito.anyString()))
                .thenReturn(userDAO);
        //act
        System.out.println(user.getEnabled());
        mockUserService.disableUser(1);
        //asser
        Mockito.verify(userDetailsManager, Mockito.times(1)).updateUser(userDAO);
    }

    @Test
    public void enableUserShould_CallRepository() {
        UserInformation user = new UserInformation();
        user.setUserName("svetlio");
        user.setPassword("123");
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn("123");
        org.springframework.security.core.userdetails.User userDAO = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), true,
                true, true, true, authorities);
        Mockito.when(userRepository.getOne(Mockito.anyInt()))
                .thenReturn(user);
        Mockito.when(userDetailsManager.loadUserByUsername(Mockito.anyString()))
                .thenReturn(userDAO);
        //act
        mockUserService.enableUser(1);
        //asser
        Mockito.verify(userDetailsManager, Mockito.times(1)).updateUser(userDAO);
    }

    @Test
    public void creatUserShould_CreatUser() {
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("vaslp");
        user.setPassword("asd");
        user.setFirstName("vas");
        user.setLastName("sil");

        //act
        mockUserService.createUser(user);
        //assert
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    @Test
    public void getAllShould_ReturnAllUsersInRepository() {
        //arrange
        UserInformation user1 = new UserInformation();
        UserInformation user2 = new UserInformation();
        UserInformation user3 = new UserInformation();
        user2.setUserName("pesho");
        user3.setUserName("toshko");
        user1.setUserName("Nadya");
        List<UserInformation> testList = new LinkedList<>();
        testList.add(user1);
        testList.add(user2);
        testList.add(user3);
        Mockito.when(userRepository.findAll())
                .thenReturn(testList);
        //assert, act
        Assert.assertEquals(testList, mockUserService.getAll());

    }

    @Test
    public void getUserByUserNameShould_ReturnUser_WhenUserExists_WithFindByUserName() {
        //arrange
        UserInformation user = new UserInformation();
        user.setUserName("gari");
        Mockito.when(userRepository.findByUserName(Mockito.anyString()))
                .thenReturn(user);
        // act
        UserInformation userToReturn = mockUserService.getByUserName("gari");
        //assert
        Assert.assertEquals(user.getUserName(), userToReturn.getUserName());
    }

    @Test
    public void UpdateUserShould_CallRepository() {
        //arrange
        UserInformation oldUser = new UserInformation();
        oldUser.setId(1);
        oldUser.setUserName("Vas");
        oldUser.setFirstName("Vasil");
        oldUser.setLastName("silia");

        UserInformation userToUpdate = new UserInformation();
        userToUpdate.setUserName("Gari");
        userToUpdate.setFirstName("Gavril");
        userToUpdate.setLastName("Samodivekov");

        Mockito.when(userRepository.getOne(1)).thenReturn(oldUser);
        //act
        mockUserService.updateUser(1, userToUpdate);
        //assert
        Mockito.verify(userRepository, Mockito.times(1)).getOne(1);
    }

    @Test
    public void UpdateUserShould_ReturnUser(){
        //arrange
        UserInformation oldUser = new UserInformation();
        oldUser.setId(1);
        oldUser.setUserName("Vas");
        oldUser.setFirstName("Vasil");
        oldUser.setLastName("silia");

        UserInformation userToUpdate = new UserInformation();
        userToUpdate.setFirstName("Gavril");
        userToUpdate.setLastName("Samodivekov");
        Mockito.when(userRepository.getOne(1)).thenReturn(oldUser);

        //act
        UserInformation  updatedUser =  mockUserService.updateUser(1,userToUpdate);
        //assert
        Assert.assertEquals(userToUpdate.getFirstName(), updatedUser.getFirstName());
    }


    @Test
    public  void GetAllPageableShould_CallRepository(){
        //Arrange
        UserInformation user1 = new UserInformation();
        user1.setUserName("Gari");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");
        UserInformation user2 = new UserInformation();
        user1.setUserName("Gari");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");
        UserInformation user3 = new UserInformation();
        user1.setUserName("Gari");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");

        List<UserInformation> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        Page<UserInformation> newPage = new PageImpl<>(list);
        Mockito.when(userRepository.findAll(PageRequest.of(1, 5))).thenReturn(newPage);
        //Act
        mockUserService.getAllPageable(PageRequest.of(1, 5));
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).findAll(PageRequest.of(1, 5));

    }


    @Test
    public  void findAllByUserNameLikeShould_CallRepository(){
        //Arrange
        UserInformation user1 = new UserInformation();
        user1.setUserName("Gari");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");
        UserInformation user2 = new UserInformation();
        user1.setUserName("Gari1");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");
        UserInformation user3 = new UserInformation();
        user1.setUserName("Gari2");
        user1.setFirstName("Gavril");
        user1.setLastName("Samodivekov");

        List<UserInformation> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        Page<UserInformation> newPage = new PageImpl<>(list);
        String username = "Gari";
        Pageable pageable =PageRequest.of(1, 5);
        //Act
        Page<UserInformation> newPage2 = mockUserService.findAllByUserNameLike(username,pageable);
        //Assert
        Assert.assertEquals(newPage.getContent().get(0).getUserName(), "Gari2");
    }
}
