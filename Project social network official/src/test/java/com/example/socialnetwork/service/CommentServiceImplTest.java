package com.example.socialnetwork.service;

import com.example.socialnetwork.exceptions.EntityBadRequestException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.CommentRepository;
import com.example.socialnetwork.services.CommentServiceImpl;
import com.example.socialnetwork.services.contracts.CommentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.example.socialnetwork.service.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

    @Mock
    CommentRepository mockRepository;

    @InjectMocks
    CommentServiceImpl commentService;


    @Test
    public void getComment_Should_Return_Comment_When_ID_Matches() {

        Comment testComment = createComment();

        Mockito.when(mockRepository.findById(1)).thenReturn(Optional.of(testComment));

        Comment comment = commentService.getCommentByID(1);

        Assert.assertEquals(1, comment.getCommentId());
    }

    @Test
    public void createComment_ShouldInvoke_repository() {
        Comment testComment = createComment();
        UserInformation testUser = createUser();
        Post testPost = createPost();

        Mockito.when(mockRepository.saveAndFlush(testComment)).thenReturn(testComment);

        Comment comment = commentService.createComment(testComment, testUser, testPost);

        Assert.assertSame(testComment, comment);

    }

    @Test
    public void getAll_Comments_Should_Return_All_Comments() {
        Comment testComment = createComment();


        Mockito.when(mockRepository.findAll()).thenReturn(Arrays.asList(testComment));

        List<Comment> comments = commentService.getComment();

        Assert.assertEquals(1, comments.size());
    }

    @Test
    public void deleteComment_Should_Delete_Comment() {
        //Act
        Comment testComment = createComment();
        commentService.deleteComment(testComment);
        //Assert
        verify(mockRepository, times(1)).delete(testComment);
    }

    @Test
    public void updateComment_ShouldUpdate_Comment() {
        //Arrange
        Comment testComment = createComment();
        testComment.setCommentId(1);
        Mockito.when(mockRepository.findById(1)).thenReturn(Optional.of(testComment));
        //Act
        commentService.updateComment(1, testComment);
        //Assert
        verify(mockRepository, times(1)).save(testComment);

    }

    @Test(expected = EntityBadRequestException.class)
    public void getCommentByID_ShouldThrow_Exception_when_Comment_Not_Found(){
        //Arrange

        Comment testComment = createComment();
        Mockito.lenient().when(mockRepository.findAll()).thenReturn(Arrays.asList(testComment));

        //Act
        Comment comment = commentService.getCommentByID(2);
    }


}
