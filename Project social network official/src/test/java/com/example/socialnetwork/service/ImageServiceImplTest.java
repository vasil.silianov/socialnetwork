package com.example.socialnetwork.service;

import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.ImageServiceImpl;
import com.example.socialnetwork.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceImplTest {

    @Mock
    UserRepository userRepository;
    @Mock
    UserService userService;

    @InjectMocks
    ImageServiceImpl imageService;


    @Test
    public void saveImageWhenCalls_UserServiceUpdate() {
        //Arrange
        UserInformation testUser = Factory.createUser();
        Mockito.when(userService.getUserById(testUser.getId())).thenReturn(testUser);
        MockMultipartFile picture = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        //act
        imageService.saveImage(testUser.getId(), picture);
        //asser
        Mockito.verify(userService, Mockito.times(1)).updateUser(testUser.getId(), testUser);
    }

    @Test
    public void convertImageShould_ReturnByteArr() throws IOException {
        //Arrange
        MockMultipartFile picture = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        Byte[] byteObject = new Byte[picture.getBytes().length];
        int i = 0;
        for (byte b : picture.getBytes()) {
            byteObject[i++] = b;
        }
        //Act
        Byte[] byteObject2 = imageService.convertImage(picture);
        //Assert
        Assert.assertEquals(byteObject.length,byteObject2.length);
    }

}
