package com.example.socialnetwork.service;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.PostServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Optional;

import static com.example.socialnetwork.service.Factory.createPost;
import static com.example.socialnetwork.service.Factory.createUser;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

    @Mock
    PostRepository mockRepository;

    @Mock
    UserRepository userRepository;
    @InjectMocks
    PostServiceImpl service;


    @Test
    public void getAllPosts_Should_ReturnPosts() {
        //Arrange
        service.getAll();

        verify(mockRepository, times(1));

    }

    @Test
    public void getAllDTO_Should_return_allDTO() {
        service.getAllDto();

        verify(mockRepository, times(1));
    }

    @Test
    public void getAllPublic_Should_return_Public_Posts() {
        service.getAllPublicFeed();

        verify(mockRepository, times(1));
    }

    @Test
    public void getPostID_Should_Return_PostID() {
        //Arrange
        Post expectedPost = createPost();
        Mockito.when(mockRepository.findById(1)).thenReturn(Optional.of(expectedPost));

        //Act
        Post test = service.getPostById(1);

        //Assert
        Assert.assertEquals("Some text", test.getPostText());
    }

    @Test(expected = PostDoesNotExistsException.class)
    public void get_PostById_Should_Return_An_Exception_When_Id_NotExists() {

        // Arrange

        Post post = createPost();
        Mockito.lenient().when(mockRepository.findAll())
                .thenReturn(Arrays.asList(post));

        // Act
        Post testPost = service.getPostById(2);
    }

    @Test
    public void createPost_Should_Invoke_Repository_Save() {
        // Arrange
        Post testPost = createPost();
        testPost.setVisibility(1);
        testPost.setEnabled(1);
        testPost.setCreatedDate(Date.from(Instant.now()));
        UserInformation testUser = createUser();

        Mockito.when(userRepository.getByUserName(testUser.getUserName())).
                thenReturn(Optional.of(testUser));
        Mockito.when(mockRepository.saveAndFlush(testPost)).thenReturn(testPost);

        //Act
        service.createPost(testPost,testUser.getUserName());

        //Assert
        verify(mockRepository, times(1)).save(testPost);
    }
    
    @Test
    public void disLikePost_Should_disLikePost_andCountLikes() {
        //Arrange
        UserInformation testUser = createUser();
        Post testPost = createPost();
        testPost.setPostText("asd");

        testPost.setLikes(new LinkedHashSet<>(1));
        testPost.getLikes().add(testUser);
        Mockito.when(mockRepository.findById(1)).thenReturn(java.util.Optional.of(testPost));

        //Act
        service.unlikePost(1, testUser);

        //Assert
        verify(mockRepository, times(1)).save(testPost);
        Assert.assertEquals(0, testPost.getLikes().size());

    }

    @Test
    public void deletePost_Should_Delete_Post() {
        // Arrange
        String[] myStringArray = new String[] {"02.12.2019", "12.06.2020"};
        Post testPost = createPost();
        Mockito.when(mockRepository.findById(1))
                .thenReturn(Optional.of(testPost));

        // Act
        service.deletePostByID(1);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteById(1);
    }


    @Test
    public void setPostPrivate_Should_Return_CorrectData() {
        // Arrange
        UserInformation testUser = createUser();
        testUser.setId(1);
        Post testPost = createPost();
        testPost.setVisibility(1);
        Mockito.when(mockRepository.findById(1))
                .thenReturn(Optional.of(testPost));

        // Act
        service.changePrivacyToPrivate(1, testUser);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(testPost);
        Assert.assertEquals(0, service.getPostById(1).getVisibility());

    }

    @Test
    public void setPostPublic_Should_Return_CorrectData() {
        // Arrange
        UserInformation testUser = createUser();
        testUser.setId(1);
        Post testPost = createPost();
        testPost.setVisibility(0);
        Mockito.when(mockRepository.findById(1))
                .thenReturn(Optional.of(testPost));

        // Act
        service.changePrivacyToPublic(1, testUser);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(testPost);
        Assert.assertEquals(1, service.getPostById(1).getVisibility());

    }


    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowException_When_User_isNot_Allowed() {
        // Arrange
        UserInformation testUser = createUser();
        testUser.setId(5);
        Post testPost = createPost();
        testPost.setVisibility(1);
        Mockito.when(mockRepository.findById(1))
                .thenReturn(Optional.of(testPost));

        // Act
        service.changePrivacyToPrivate(1, testUser);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(testPost);
        Assert.assertEquals(0, service.getPostById(1).getVisibility());

    }

}
