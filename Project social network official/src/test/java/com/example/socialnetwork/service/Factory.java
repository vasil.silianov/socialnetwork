package com.example.socialnetwork.service;

import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;

import java.util.HashSet;

public class Factory {

//    public static Beer createBeer (){
//        Beer beer =  new Beer (1,"Brand new", "Best beer ever with chocolate.", 4.5);
////        beer.setBrewery(new Brewery(12,"Strumsko"));
////        beer.getCreatedBy().setId(1);
//
//        return beer;
//    }

    public static Post createPost() {

        Post post = new Post(1,
                new UserInformation(1, "Johny", "Bravo",
                        "JB", "1234", "mail@yahoo.com"),
                "Some text");
        post.setLikes(new HashSet<>());

        return post;
    }

    public static UserInformation createUser() {
        UserInformation user = new UserInformation(1, "Johny", "Bravo",
                "JB", "1234", "mail@yahoo.com");
        return user;
    }

    public static Comment createComment() {
        Comment comment = new Comment(1, createPost(), createUser(), "some comment");
        return comment;
    }


}
