package com.example.socialnetwork.service;

import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.models.UserRelationship;
import com.example.socialnetwork.repositories.UserRelationshipRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.UserRelationshipServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserRelationshipServiceImplTests {
    @InjectMocks
    UserRelationshipServiceImpl mockUserRelationshipService;

    @Mock
    UserRepository userRepository;

    @Mock
    UserRelationshipRepository userRelationshipRepository;



    @Test
    public void getAllFriendsShould_returnFriendList(){
        //arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);


        UserRelationship relationship = new UserRelationship();
        relationship.setStatus("Approved");
        relationship.setRequester(userRequester);
        relationship.setRequestee(userRequestee);
        relationship.setId(1);

        List<UserRelationship> relationships = new ArrayList<>();
        relationships.add(relationship);

    Mockito.when(userRelationshipRepository.filterRequester(Mockito.anyInt())).thenReturn(relationships);
        //act
        List<UserInformation> relationshipsCompare =  mockUserRelationshipService.getAllFriends(2);
        //assert
        Assert.assertEquals(userRequestee.getUserName(),relationshipsCompare.get(0).getUserName());
    }


    @Test
    public void sendFriendRequestShould_CallRepository(){
        //arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);

        UserRelationship relationshipTestOne = new UserRelationship();
        UserRelationship relationshipTestTwo = new UserRelationship();
        relationshipTestOne.setStatus("Waiting");
        relationshipTestOne.setRequester(userRequester);
        relationshipTestOne.setRequestee(userRequestee);
        relationshipTestOne.setId(1);

        relationshipTestTwo.setStatus("Pending");
        relationshipTestTwo.setRequester(userRequestee);
        relationshipTestTwo.setRequestee(userRequester);
        relationshipTestTwo.setId(2);

        Mockito.when(userRepository.getOne(2)).thenReturn(userRequester);
        Mockito.when(userRepository.getOne(1)).thenReturn(userRequestee);

        //act
        mockUserRelationshipService.sendFriendRequest(userRequester.getId(),userRequestee.getId());
        // assert
        Mockito.verify(userRepository,Mockito.times(1)).getOne(userRequester.getId());
    }

    @Test
    public void answerFriendRequestShould_WithApprove(){
        //Arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);

        UserRelationship relationshipTestOne = new UserRelationship();
        UserRelationship relationshipTestTwo = new UserRelationship();
        relationshipTestOne.setStatus("Waiting");
        relationshipTestOne.setRequester(userRequester);
        relationshipTestOne.setRequestee(userRequestee);
        relationshipTestOne.setId(1);

        relationshipTestTwo.setStatus("Pending");
        relationshipTestTwo.setRequester(userRequestee);
        relationshipTestTwo.setRequestee(userRequester);
        relationshipTestTwo.setId(2);

        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequester.getId(),userRequestee.getId())).thenReturn(relationshipTestOne);
        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequestee.getId(),userRequester.getId())).thenReturn(relationshipTestTwo);
        //Act
        mockUserRelationshipService.answerFriendRequest(2,1,"Approved");

        //assert
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).save(relationshipTestOne);
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).save(relationshipTestTwo);
        Assert.assertEquals(relationshipTestOne.getStatus(),"Approved");
        Assert.assertEquals(relationshipTestTwo.getStatus(),"Approved");
    }


    @Test
    public void answerFriendRequestShould_WithReject(){
        //Arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);

        UserRelationship relationshipTestOne = new UserRelationship();
        UserRelationship relationshipTestTwo = new UserRelationship();
        relationshipTestOne.setStatus("Waiting");
        relationshipTestOne.setRequester(userRequester);
        relationshipTestOne.setRequestee(userRequestee);
        relationshipTestOne.setId(1);

        relationshipTestTwo.setStatus("Pending");
        relationshipTestTwo.setRequester(userRequestee);
        relationshipTestTwo.setRequestee(userRequester);
        relationshipTestTwo.setId(2);

        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequester.getId(),userRequestee.getId())).thenReturn(relationshipTestOne);
        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequestee.getId(),userRequester.getId())).thenReturn(relationshipTestTwo);
        //Act
        mockUserRelationshipService.answerFriendRequest(2,1,"Reject");
        //assert
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).delete(relationshipTestOne);
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).delete(relationshipTestTwo);
        Assert.assertEquals(relationshipTestOne.getStatus(),"Waiting");
        Assert.assertEquals(relationshipTestTwo.getStatus(),"Pending");
    }

    @Test
    public void unfriendShould_DeleteFromDB(){
        //Arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);

        UserRelationship relationshipTestOne = new UserRelationship();
        UserRelationship relationshipTestTwo = new UserRelationship();
        relationshipTestOne.setStatus("Approved");
        relationshipTestOne.setRequester(userRequester);
        relationshipTestOne.setRequestee(userRequestee);
        relationshipTestOne.setId(1);
        relationshipTestTwo.setStatus("Approved");
        relationshipTestTwo.setRequester(userRequestee);
        relationshipTestTwo.setRequestee(userRequester);
        relationshipTestTwo.setId(2);

        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequester.getId(),userRequestee.getId())).thenReturn(relationshipTestOne);
        Mockito.when(userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userRequestee.getId(),userRequester.getId())).thenReturn(relationshipTestTwo);
        //Act
        mockUserRelationshipService.unfriend(2,1);
        //assert
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).delete(relationshipTestOne);
        Mockito.verify(userRelationshipRepository,Mockito.times(1)).delete(relationshipTestTwo);
    }


    @Test
    public void requestForFriendShipsShould_ReturnListOFRequestForFriendship(){
        //arrange
        UserInformation userRequester = new UserInformation();
        UserInformation userRequestee = new UserInformation();
        userRequestee.setUserName("vasko");
        userRequestee.setId(1);
        userRequester.setUserName("gari");
        userRequester.setId(2);


        UserRelationship relationshipTest = new UserRelationship();
        relationshipTest.setStatus("Pending");
        relationshipTest.setRequester(userRequester);
        relationshipTest.setRequestee(userRequestee);
        relationshipTest.setId(1);

        List<UserRelationship> relationshipsTest = new ArrayList<>();
        relationshipsTest.add(relationshipTest);

        Mockito.when(userRelationshipRepository.filterRequester(Mockito.anyInt())).thenReturn(relationshipsTest);
        //act
        List<UserInformation> relationshipsCompare =  mockUserRelationshipService.requestForFriendShips(2);
        //assert
        Assert.assertEquals(userRequestee.getUserName(),relationshipsCompare.get(0).getUserName());
    }

    //Създаваш 2 userRelationshipObjects
    //Един тест, когато стойноста е Reject и един когато не е
    //2 Mocka na userRelationshipRepository, където връщаш създадените обекти
    //Правиш си акт за метода със mockUserRelationshipService.answerFriendRequest()
    //След това правиш assert частта, която ще е с Mockito.verify(userRelationshipRepository, Mockito.times(1)).delete или save зависи, кой сценарии тестваш


}
