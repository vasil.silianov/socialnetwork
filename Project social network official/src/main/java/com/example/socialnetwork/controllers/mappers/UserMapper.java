package com.example.socialnetwork.controllers.mappers;

import com.example.socialnetwork.models.DTO.UserDTO;
import com.example.socialnetwork.models.UserInformation;

public class UserMapper {



    public static  UserInformation userMapper(UserDTO userDTO){
        UserInformation userDetails = new UserInformation();
        userDetails.setEmail(userDTO.getEmail());
        userDetails.setUserName(userDTO.getUserName());
        userDetails.setFirstName(userDTO.getFirstName());
        userDetails.setPassword(userDTO.getPassword());
        userDetails.setLastName(userDTO.getLastName());
        return userDetails;
    }

}
