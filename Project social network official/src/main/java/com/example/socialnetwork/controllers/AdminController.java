package com.example.socialnetwork.controllers;

import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.PostService;
import com.example.socialnetwork.services.contracts.UserRelationshipService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
public class AdminController {

    private UserService userService;
    private UserDetailsManager userDetailsManager;
    private UserRepository userRepository;
    private PostService postService;
    private UserRelationshipService userRelationshipService;

    @Autowired
    public AdminController(UserRepository userRepository,UserService userService,UserDetailsManager userDetailsManager,
                           PostService postService,  UserRelationshipService userRelationshipService) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.userRepository = userRepository;
        this.postService = postService;
        this.userRelationshipService = userRelationshipService;
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }


    @GetMapping("/admin/users")
    public  String allUser(Model model){
        model.addAttribute("users",userService.getAll());
        return "admin-panel-for-users";
    }

    @GetMapping("/admin/user/{name}")
    public String disable( @PathVariable String name){
        UserInformation userInformation = userService.getByUsername(name);
        userService.disableUser(userInformation.getId());
        User  user = (User) userDetailsManager.loadUserByUsername(name);
        return "admin";
    }

    @GetMapping("/admin/user/enable/{name}")
    public String enable( @PathVariable String name){
        UserInformation userInformation = userService.getByUsername(name);
        userService.enableUser(userInformation.getId());
        User  user = (User) userDetailsManager.loadUserByUsername(name);
        return "admin";
    }

    @GetMapping("/admin/userfeed")
    public String adminFeed(Model model, Principal principal){

        UserInformation user = userService.getCurrentUser(principal);
        model.addAttribute("user", userService.getByUserName(principal.getName()));
        model.addAttribute("posts", postService.postsCreatedBy(user));
        model.addAttribute("allposts", postService.getAll());
        model.addAttribute("postDTO", new PostDTO());
        model.addAttribute("comment", new Comment());
        model.addAttribute("friendlist",userRelationshipService.getAllFriends(user.getId()));
        model.addAttribute("friendrequests",userRelationshipService.requestForFriendShips(user.getId()));
        model.addAttribute("viral",postService.currentViralPost());
        return "admin-panel-for-feed";
    }

    @GetMapping("/admin/userfeed/{postId}")
    public String deletePost(@PathVariable int postId){
        postService.deletePostByID(postId);
        return "admin";
    }

}
