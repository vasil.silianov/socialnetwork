package com.example.socialnetwork.controllers.rest;

import com.example.socialnetwork.exceptions.CommentDoesNotExistsException;
import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.DTO.CommentDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.services.contracts.CommentService;
import com.example.socialnetwork.services.contracts.UserService;
import io.swagger.models.Model;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {

    private CommentService commentService;
    private UserService userService;

    @Autowired
    public CommentRestController(CommentService commentService, UserService userService) {
        this.commentService = commentService;
        this.userService = userService;
    }



    @GetMapping("/")
    public List<Comment> getAllComments(){
        return commentService.getComment();
    }

    @GetMapping("/{commentID}")
    public Comment getCommentByID(@PathVariable int commentID){
        try {
            return commentService.getCommentByID(commentID);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{commentID}")
    public void deleteComment(@PathVariable int commentID){

        try{
            Comment comment = commentService.getCommentByID(commentID);
              commentService.deleteComment(comment);
        }catch (CommentDoesNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


}
