package com.example.socialnetwork.exceptions;

public class EntityBadRequestException extends RuntimeException {
    public  EntityBadRequestException(String msg) {
        super(msg);
    }
}
