package com.example.socialnetwork.exceptions;

public class PostPrivacyAlreadyChanged extends RuntimeException{
    public PostPrivacyAlreadyChanged(String message) {
        super(message);
    }
}
