package com.example.socialnetwork.exceptions;


public  class EntityNotFoundException extends RuntimeException{
public  EntityNotFoundException(String msg) {
        super(msg);
    }
}