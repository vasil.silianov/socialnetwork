package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    public static final String USER_NOT_FOUND = "Username doesn't exists!";
    public static final String USER_IS_ALREADY_ENABLED = "is already enabled";
    public static final String USER_IS_ALREADY_DISABLED = "is already disabled";
    public static final String USER_WAS_DISABLED = "user %s was disabled";
    public static final String USER_WAS_ENABLED = "user %s was enabled";

    private UserRepository userRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserDetailsManager userDetailsManager,
                           PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Collection<UserInformation> getAll() {
        return userRepository.findAll();
    }

    @Override
    public void createUser(UserInformation user) {
        userRepository.save(user);
    }

    @Override
    public UserInformation getUserById(int userID) {
        Optional<UserInformation> user = userRepository.findById(userID);
        if (!user.isPresent()) {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }
        return user.get();
    }

    @Override
    public UserInformation getCurrentUser(Principal principal) {
        return getByUsername(principal.getName());
    }

    @Override
    public UserInformation getByUsername(String username) {
        return userRepository.getByUserName(username)
                .orElseThrow(() -> new EntityNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public String disableUser(int id) {
        userDetailsManager.updateUser(disable(id));
        return String.format(USER_WAS_DISABLED,disable(id).getUsername() );
    }

    private User disable(int id) {
        UserInformation user = userRepository.getOne(id);

        user.setEnabled(0);
        userRepository.save(user);
        User userToBeDisabled = (User) userDetailsManager.loadUserByUsername(user.getUserName());
        return new User(userToBeDisabled.getUsername(),
                passwordEncoder.encode(user.getPassword()), false,
                true, true, true, userToBeDisabled.getAuthorities());
    }

    @Override
    public UserInformation getByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public UserInformation updateUser(int id, UserInformation newUser) {
        UserInformation user = userRepository.getOne(id);
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setEmail(newUser.getEmail());
        userRepository.save(user);
        return user;
    }

    @Override
    public Page<UserInformation> getAllPageable(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public Page<UserInformation> findAllByUserNameLike(String username, Pageable pageable) {
        return userRepository.findAllByUserNameLike("%" + username + "%", pageable);
    }

    @Override
    public String enableUser(int id) {
        userDetailsManager.updateUser(enable(id));
        return String.format(USER_WAS_ENABLED,disable(id).getUsername() );
    }

    private User enable(int id) {
        UserInformation user = userRepository.getOne(id);
        if (user.getEnabled() ==1){
            throw  new IllegalArgumentException(USER_IS_ALREADY_ENABLED);
        }
        user.setEnabled(1);
        userRepository.save(user);
        User userToBeDisabled = (User) userDetailsManager.loadUserByUsername(user.getUserName());
        return new User(userToBeDisabled.getUsername(),
                passwordEncoder.encode(user.getPassword()), true,
                true, true, true, userToBeDisabled.getAuthorities());
    }
}

