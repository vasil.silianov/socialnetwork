package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.UserInformation;

import java.util.List;

public interface UserRelationshipService {
    List<UserInformation> getAllFriends(int currentUser);

    void sendFriendRequest(int currentUser, int userToFriend);

    void unfriend(int currentUser, int userToUnfriend);

    void answerFriendRequest(int current, int sender, String answer);

    List<UserInformation> requestForFriendShips(int currentUser);
}
