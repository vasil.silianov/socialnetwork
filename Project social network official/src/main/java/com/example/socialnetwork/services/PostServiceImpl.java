package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.DuplicateEntityException;
import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.exceptions.PostDoesNotExistsException;
import com.example.socialnetwork.exceptions.PostPrivacyAlreadyChanged;
import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.PostRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    public static final String POST_NOT_FOUND = "There isn't any post with ID %d";
    public static final String DUPLICATE_LIKE = "Post already liked!";
    public static final String NO_LIKE_FOUND = "Like doesn't exists!";
    public static final String ALREADY_PRIVATE = "Post privacy already private!";
    public static final String ALREADY_PUBLIC = "Post privacy already public!";
    public static final String NOT_ALLOWED = "You are not allowed this operation!";


    private PostRepository postRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public List<Post> getAll() {
        return postRepository.findAllByOrderByCreatedDateDesc();
    }

    @Override
    public List<PostDTO> getAllDto() {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(post -> modelMapper.map(post, PostDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Post getPostById(int postID) {
        Optional<Post> post = postRepository.findById(postID);
        if (!post.isPresent()) {
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND, postID));
        }
        return post.get();
    }

    @Override
    public Post createPost(Post post, String username) {
        UserInformation user = userRepository.findByUserName(username);
        post.setUser(user);
        post.setVisibility(1);
        post.setEnabled(1);
        post.setCreatedDate(Date.from(Instant.now()));
        postRepository.save(post);
        return post;
    }

    @Override
    public void deletePostByID(int postID) {
        Optional<Post> newPost = postRepository.findById(postID);
        if (!newPost.isPresent()) {
            throw new PostDoesNotExistsException(String.format(POST_NOT_FOUND, postID));
        }
        postRepository.deleteById(postID);
    }

    @Override
    public List<Post> postsCreatedBy(UserInformation user) {
        return postRepository.findAllByUserOrderByCreatedDateDesc(user);
    }

    @Override
    public int likePost(int postID, UserInformation user) {
        Post post = getPostById(postID);
        if (post.getLikes().stream().anyMatch(x -> x.getId() == user.getId())) {
            throw new DuplicateEntityException(DUPLICATE_LIKE);
        }
        post.getLikes().add(user);
        postRepository.save(post);
        return post.getLikes().size();
    }

    @Override
    public int unlikePost(int postID, UserInformation user) {
        Post post = getPostById(postID);

        if (post.getLikes().stream().noneMatch(x -> x.getId() == user.getId())) {
            throw new EntityNotFoundException(NO_LIKE_FOUND);
        }
        post.getLikes().remove(user);
        postRepository.save(post);
        return post.getLikes().size();
    }

    @Override
    public String changePrivacyToPublic(int postID, UserInformation user) {
        Post post = getPostById(postID);

        if (getPostById(postID).getUser().getId() != user.getId()) {
            throw new EntityNotFoundException(NOT_ALLOWED);
        }

        if (post.getVisibility() == 1) {
            throw new PostPrivacyAlreadyChanged(ALREADY_PUBLIC);
        }

        post.setVisibility(1);
        postRepository.save(post);
        return " Public";
    }

    @Override
    public List<Post> getAllPublicFeed() {
        return postRepository.getAllPublicPosts();
    }

    @Override
    public String changePrivacyToPrivate(int postID, UserInformation user) {
        Post post = getPostById(postID);

        if (getPostById(postID).getUser().getId() != user.getId()) {
            throw new EntityNotFoundException(NOT_ALLOWED);
        }

        if (post.getVisibility() == 0) {
            throw new PostPrivacyAlreadyChanged(ALREADY_PRIVATE);
        }

        post.setVisibility(0);
        postRepository.save(post);
        return " Private";
    }

    public List<Post> getLatest3DaysPosts() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -3);
        Date beginningOfTime = now.getTime();
        return postRepository.findPostsByCreatedDateIsAfter(beginningOfTime);
    }

    public Post currentViralPost() {
        List<Post> lastFewPosts = getLatest3DaysPosts();
        int totalSum;
        int biggest = 0;
        Post viral = new Post();
        for (int i = 0; i < lastFewPosts.size(); i++) {
            totalSum = 0;
            totalSum += lastFewPosts.get(i).getLikes().size();
            totalSum += lastFewPosts.get(i).getComments().size();
            if (totalSum > biggest) {
                biggest = totalSum;
                viral = lastFewPosts.get(i);
            }
        }
        return viral;
    }
}
