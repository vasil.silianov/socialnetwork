package com.example.socialnetwork.services.contracts;


import com.example.socialnetwork.models.Comment;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;

import java.util.List;

public interface CommentService {

    Comment createComment(Comment comment,UserInformation user, Post post);


    Comment getCommentByID(int commentId);

    Comment updateComment(int commentID, Comment comment);

    void deleteComment(Comment comment);

    List<Comment> getComment();
}
