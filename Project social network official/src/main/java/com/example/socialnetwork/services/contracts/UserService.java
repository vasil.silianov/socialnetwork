package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.UserInformation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.Collection;

public interface UserService {

    Collection<UserInformation> getAll();

    void createUser(UserInformation user);

    UserInformation getUserById(int userID);

    UserInformation getCurrentUser(Principal principal);

    UserInformation getByUsername(String username);

    String disableUser(int id);

    String enableUser(int id);

    UserInformation getByUserName(String username);

    UserInformation updateUser(int id, UserInformation user);

    Page<UserInformation> getAllPageable(Pageable pageable);

    Page<UserInformation> findAllByUserNameLike(String username, Pageable pageable);



}
