package com.example.socialnetwork.services;

import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.ImageService;
import com.example.socialnetwork.services.contracts.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@Service
public class ImageServiceImpl implements ImageService {

    UserRepository userRepository;
    UserService userService;


    public ImageServiceImpl(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }


    @Transactional
    @Override
    public void saveImage(int id, MultipartFile file) {
        try {
            UserInformation user = userService.getUserById(id);
            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;
            for (byte b : file.getBytes()) {
                byteObjects[i++] = b;
            }
            user.setPicture(byteObjects);
            userService.updateUser(id, user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Byte[] convertImage(MultipartFile image) throws IOException {

        Byte[] byteObject = new Byte[image.getBytes().length];
        int i = 0;
        for (byte b : image.getBytes()) {
            byteObject[i++] = b;
        }
        return byteObject;
    }
}
