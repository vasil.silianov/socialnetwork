package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.DTO.PostDTO;
import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;

import java.util.List;

public interface PostService {
    List<Post> getAll();

    List<PostDTO> getAllDto();

    Post getPostById(int postID);

     Post createPost(Post post, String username);

     void deletePostByID(int postID);

     List<Post> postsCreatedBy(UserInformation user);

    int likePost(int postID, UserInformation user);

    int unlikePost(int postID,UserInformation user);

    List<Post> getLatest3DaysPosts();


    String changePrivacyToPrivate(int postID, UserInformation user);

    String changePrivacyToPublic(int postID, UserInformation user);

    List<Post> getAllPublicFeed();

    Post currentViralPost();
}
