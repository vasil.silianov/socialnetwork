package com.example.socialnetwork.services;

import com.example.socialnetwork.models.UserInformation;
import com.example.socialnetwork.models.UserRelationship;
import com.example.socialnetwork.repositories.UserRelationshipRepository;
import com.example.socialnetwork.repositories.UserRepository;
import com.example.socialnetwork.services.contracts.UserRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRelationshipServiceImpl implements UserRelationshipService {

    private UserRelationshipRepository userRelationshipRepository;
    private UserRepository userRepository;

    @Autowired
    public UserRelationshipServiceImpl(UserRelationshipRepository userRelationshipRepository, UserRepository userRepository) {
        this.userRelationshipRepository = userRelationshipRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<UserInformation> getAllFriends(int currentUser) {
        List<UserInformation> friendList = new ArrayList<>();
        List<UserRelationship> relationships = userRelationshipRepository.filterRequester(currentUser);

        for (int i = 0; i < relationships.size(); i++) {
            if (relationships.get(i).getStatus().equals("Approved")) {
                friendList.add(relationships.get(i).getRequestee());
            }
        }
        return friendList;
    }

    /**
     * @param currentUser  this is the ID of the user who send the request for friendship
     * @param userToFriend this is the ID of the user who's pending ...
     */
    @Override
    public void sendFriendRequest(int currentUser, int userToFriend) {
        UserRelationship relationshipWaiting = new UserRelationship();
        UserRelationship relationshipPending = new UserRelationship();
        UserInformation requester = userRepository.getOne(currentUser);
        UserInformation requestee = userRepository.getOne(userToFriend);

        relationshipWaiting.setRequester(requester);
        relationshipWaiting.setRequestee(requestee);
        relationshipWaiting.setStatus("Waiting");

        relationshipPending.setRequester(requestee);
        relationshipPending.setRequestee(requester);
        relationshipPending.setStatus("Pending");

        userRelationshipRepository.save(relationshipWaiting);
        userRelationshipRepository.save(relationshipPending);
    }

    @Override
    public void unfriend(int currentUser, int userToUnfriend) {
        UserRelationship userRelationshipOne = userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(currentUser, userToUnfriend);
        UserRelationship userRelationshipTwo = userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(userToUnfriend, currentUser);

        userRelationshipRepository.delete(userRelationshipOne);
        userRelationshipRepository.delete(userRelationshipTwo);
    }

    @Override
    public void answerFriendRequest(int currentUser, int sender, String answer) {
        UserRelationship userRelationshipPending = userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(sender, currentUser);
        UserRelationship userRelationshipWaiting = userRelationshipRepository.findUserRelationshipByRequesterIdAndRequesteeId(currentUser, sender);
        if ("Reject".equalsIgnoreCase(answer)) {
            userRelationshipRepository.delete(userRelationshipPending);
            userRelationshipRepository.delete(userRelationshipWaiting);
            return;
        }
        userRelationshipPending.setStatus(answer);
        userRelationshipWaiting.setStatus(answer);
        userRelationshipRepository.save(userRelationshipPending);
        userRelationshipRepository.save(userRelationshipWaiting);
    }

    @Override
    public List<UserInformation> requestForFriendShips(int currentUser) {
        List<UserInformation> request = new ArrayList<>();
        List<UserRelationship> relationships = userRelationshipRepository.filterRequester(currentUser);
        for (int i = 0; i < relationships.size(); i++) {
            if (relationships.get(i).getStatus().equals("Pending")) {
                request.add(relationships.get(i).getRequestee());
            }
        }
        return request;
    }
}
