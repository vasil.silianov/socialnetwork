package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> findAllByUserOrderByCreatedDateDesc(UserInformation user);

    List<Post> findAllByOrderByCreatedDateDesc();

    List<Post> findPostsByCreatedDateIsAfter(Date dateBeginningOfPosts);

    @Query("select p from Post p where p.visibility=1 order by p.createdDate desc ")
    List<Post> getAllPublicPosts();
}
