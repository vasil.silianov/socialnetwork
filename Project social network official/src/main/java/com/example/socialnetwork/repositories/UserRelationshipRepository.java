package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.UserRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRelationshipRepository extends JpaRepository<UserRelationship, Integer> {

    @Query("SELECT r FROM UserRelationship r WHERE r.requester.id = :id")
    List<UserRelationship> filterRequester(@Param("id") int id);

    @Query("SELECT r FROM UserRelationship r WHERE r.requester.id = :id")
    List<UserRelationship> filterUserTwo(@Param("id") int id);

    UserRelationship findUserRelationshipByRequesterIdAndRequesteeId(int requester, int requestee);
}
