package com.example.socialnetwork.repositories;

import com.example.socialnetwork.models.Post;
import com.example.socialnetwork.models.UserInformation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserInformation, Integer> {


    Optional<UserInformation> getByUserName(String name);

    Optional<UserInformation> findById(int userID);

    UserInformation findByUserName(String username);


    Page<UserInformation> findAllByUserNameLike(String username, Pageable pageable);
}
