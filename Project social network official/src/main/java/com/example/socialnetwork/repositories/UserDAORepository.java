package com.example.socialnetwork.repositories;


import com.example.socialnetwork.models.UserDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDAORepository extends JpaRepository<UserDAO, String> {
}
