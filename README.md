Final Project - Social Network
by Vasil Silanov & Georgi Kulov

Link to [Trello](https://trello.com/b/KYu4SbKY/final-project-social-network)

**Covid 19 
Support Network**


Project description:

Covid 19 Social Network is a web application developed as a final project for Telerik Academy. 
The app is directed mainly towards the people that have the virus or had the virus and 
are healthy now. Only people that have been registered in the minisitri of health
data base can register in this social network. They can interact with other users 
and share their experience, also they can choose if their post are private 
meaning only for registered users or public where everyone can view their posts.
The purpose of the app is to  share first hand information and to see how 
different people coupe with the virus.

Technologies used in the project:
Backend: Java 8 – Spring Boot – Hibernate – Spring Data JPA – MariaDB
Frontend: Spring MVC (Thymeleaf) – Spring Security – HTML 5 – CSS 3 –
Swagger 2 (UI) – Bootstrap